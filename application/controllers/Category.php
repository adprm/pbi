<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Category extends CI_Controller {

    // consturct
    public function __construct()
    {
        parent::__construct();
        is_logged_in();
        // load model
        $this->load->model('Category_Model');
    }

    // index category
	public function index()
	{
		// set data
        $data['title'] = 'Kategori';
        $data['user'] = $this->db->get_where('user', ['email' => $this->session->userdata('email')])->row_array();
        $data['category'] = $this->db->order_by("id", "DESC");
        $data['category'] = $this->Category_Model->getAll();
		// load view
		$this->load->view('templates/overview_header', $data);
		$this->load->view('templates/overview_sidebar');
		$this->load->view('templates/overview_topbar', $data);
		$this->load->view('category/index', $data);
		$this->load->view('templates/overview_footer');
    }
    
    // add category
    public function add()
    {
        // validation
        $this->form_validation->set_rules('type', 'Tipe', 'required', [
            'required' => 'Tipe harus di isi!'
        ]);
        $this->form_validation->set_rules('title', 'Nama Kategori', 'required', [
            'required' => 'Nama kategori harus di isi!'
        ]);

        if ($this->form_validation->run() == false) {
            // load view
            $this->load->view('templates/overview_header', $data);
            $this->load->view('templates/overview_sidebar');
            $this->load->view('templates/overview_topbar');
            $this->load->view('category/index', $data);
            $this->load->view('templates/overview_footer');
            $this->session->set_flashdata('message', '<div class="alert alert-danger" role="alert">
            Gagal Menambahkan Data!</div>');
            redirect('Category');
        } else {
            $this->Category_Model->save();
            $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">
            Berhasil Menambahkan Data!</div>');
            redirect('Category');
        }
    }

    // edit category
    public function edit($id = null)
    {
        // set data
        $data['title'] = "Ubah Kategori";
        $data['user'] = $this->db->get_where('user', ['email' => $this->session->userdata('email')])->row_array();
        $category = $this->Category_Model;
        $data['category'] = $category->getById($id);
        // validation id
        if (!isset($id)) redirect('Category');
        if (!$data['category']) show_404();
        // validation
        $this->form_validation->set_rules('type', 'Tipe', 'required', [
            'required' => 'Tipe harus di isi!'
        ]);
        $this->form_validation->set_rules('title', 'Nama Kategori', 'required', [
            'required' => 'Nama kategori harus di isi!'
        ]);

        if ($this->form_validation->run() == false) {
            // load view
            $this->load->view('templates/overview_header', $data);
            $this->load->view('templates/overview_sidebar');
            $this->load->view('templates/overview_topbar');
            $this->load->view('category/edit_category', $data);
            $this->load->view('templates/overview_footer');
            $this->session->set_flashdata('message', '<div class="alert alert-danger" role="alert">
            Data Gagal Diubah!</div>');
        } else {
            $category->update();
            $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">
            Data Berhasil Diubah!</div>');
            redirect('Category');
        }
    }

    // delete category
    public function delete($id = null)
    {
        if (!isset($id)) show_404();
        $category = $this->Category_Model;
        if ($category->delete($id)) {
            // delete category id
            $this->db->delete('sub_category', ['category_id' => $id]);
            // session delete data
            $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">
            Data Berhasil Di Hapus!</div>');
            redirect('Category');
        }
    }
}

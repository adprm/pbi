<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Configuration extends CI_Controller {

    // consturct
    public function __construct()
    {
        parent::__construct();
        is_logged_in();
        $this->load->model('Configuration_Model');
	}

    // function index
    public function index($id = 1)
    {
        // var configuration
        $configuration = $this->Configuration_Model;
        // set data
        $data['title'] = 'Konfigurasi';
        $data['user'] = $this->db->get_where('user', ['email' => $this->session->userdata('email')])->row_array();
        $data['configuration'] = $configuration->getById($id);
        // validation id
        if (!isset($id)) redirect('Configuration');
        if (!$data['configuration']) show_404();

        // validation
        $this->form_validation->set_rules('interest_loan_costs', 'Bunga kewajiban pinjaman', 'required', [
            'required' => 'Bunga kewajiban pinjaman harus di isi!'
        ]);

        if ($this->form_validation->run() == false) {
            // load view
            $this->load->view('templates/overview_header', $data);
            $this->load->view('templates/overview_sidebar');
            $this->load->view('templates/overview_topbar', $data);
            $this->load->view('configuration/index', $data);
            $this->load->view('templates/overview_footer');
        } else {
             // var post
            $post = $this->input->post();
            // data array
            $data = [
                'id' => $post['id'],
                'interest_loan_costs' => $post['interest_loan_costs'],
                'acceptance_value' => $post['acceptance_value'],
                'percent_pajak' => $post['percent_pajak'],
                'discount_rate' => $post['discount_rate'],
            ];
            // var id
            $id = $post['id'];
            // call model
            $configuration->update($data, $id);
            // set session
            $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">
            Data Berhasil Diubah!</div>');
            // redirect
            redirect('Configuration');
        }

    }
}
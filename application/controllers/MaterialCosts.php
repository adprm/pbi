<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class MaterialCosts extends CI_Controller {

    // consturct
    public function __construct()
    {
        parent::__construct();
        is_logged_in();
        // load model
        $this->load->model('MaterialCosts_Model');
        $this->load->model('Global_Model');
    }

    // function index materialcosts
	public function index()
	{
        // subtotal bahan baku
        $data['stbahanbaku'] = $this->Global_Model->biayaBahanBaku();
		// set data
        $data['title'] = 'Biaya Bahan Baku & Utilitas';
        $data['user'] = $this->db->get_where('user', ['email' => $this->session->userdata('email')])->row_array();
        $data['materialcosts'] = $this->MaterialCosts_Model->getAll();
        $data['investmentcosts'] = $this->db->get('investment_costs')->result_array();
		// load view
		$this->load->view('templates/overview_header', $data);
		$this->load->view('templates/overview_sidebar');
		$this->load->view('templates/overview_topbar', $data);
		$this->load->view('materialcosts/index', $data);
		$this->load->view('templates/overview_footer');
    }
    
    // function add materialcosts
    public function add()
    {
        $this->form_validation->set_rules('title', 'Deskripsi', 'required', [
            'required' => 'Deskripsi harus di isi!'
        ]);

        if ($this->form_validation->run() == false) {
            // load view
            $this->load->view('templates/overview_header', $data);
            $this->load->view('templates/overview_sidebar');
            $this->load->view('templates/overview_topbar');
            $this->load->view('materialcosts/index', $data);
            $this->load->view('templates/overview_footer');
            $this->session->set_flashdata('message', '<div class="alert alert-danger" role="alert">
            Gagal Menambahkan Data!</div>');
            redirect('MaterialCosts');
        } else {
            // var post
            $post = $this->input->post();
            // set timezone
            date_default_timezone_set("Asia/Jakarta");
            // logic costs year
            if ($post['total'] == 0) {
                $total = $post['total'] + $post['price_unit'];
            } else {
                $total = $post['total'] * $post['price_unit'];
            }
            // biaya / tahun
            $costsyear = $total * 24 * 12;
            // data array
            $data = [
                'title' => $post['title'],
                'total' => $post['total'],
                'price_unit' => $post['price_unit'],
                'costs_year' => $costsyear,
                'created_at' => date("y-m-d h:i:sa"),
                'updated_at' => date("y-m-d h:i:sa"),
            ];
            // call model
            $this->MaterialCosts_Model->save($data);
            // set session
            $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">
            Berhasil Menambahkan Data!</div>');
            // redirect
            redirect('MaterialCosts');
        }
    }

    // edit materialcosts
    public function edit($id = null)
    {
        // set data
        $data['title'] = "Ubah Biaya Baku & Utilitas";
        $data['user'] = $this->db->get_where('user', ['email' => $this->session->userdata('email')])->row_array();
        $materialcosts = $this->MaterialCosts_Model;
        $data['materialcosts'] = $materialcosts->getById($id);
        $data['investmentcosts'] = $this->db->get('investment_costs')->result_array();
        
        // validation id
        if (!isset($id)) redirect('MaterialCosts');
        if (!$data['materialcosts']) show_404();

        $this->form_validation->set_rules('title', 'Deskripsi', 'required', [
            'required' => 'Deskripsi harus di isi!'
        ]);

        if ($this->form_validation->run() == false) {
            // load view
            $this->load->view('templates/overview_header', $data);
            $this->load->view('templates/overview_sidebar');
            $this->load->view('templates/overview_topbar');
            $this->load->view('materialcosts/edit_materialcosts', $data);
            $this->load->view('templates/overview_footer');
            $this->session->set_flashdata('message', '<div class="alert alert-danger" role="alert">
            Data Gagal Diubah!</div>');
        } else {
            // var post
            $post = $this->input->post();
            // set timezone
            date_default_timezone_set("Asia/Jakarta");
            // logic costs year
            if ($post['total'] == 0) {
                $total = $post['total'] + $post['price_unit'];
            } else {
                $total = $post['total'] * $post['price_unit'];
            }
            
            // biaya pertahun 
            $costsyear = $total * 24 * 12;
            // data array
            $data = [
                'id' => $post['id'],
                'title' => $post['title'],
                'total' => $post['total'],
                'price_unit' => $post['price_unit'],
                'costs_year' => $costsyear,
                'created_at' => $post['created_at'],
                'updated_at' => date("y-m-d h:i:sa"),
            ];
            // var id
            $id = $post['id'];
            // call model
            $materialcosts->update($data, $id);
            // set session
            $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">
            Data Berhasil Diubah!</div>');
            // redirect
            redirect('MaterialCosts');
        }
    }

    public function delete($id = null)
    {
        if (!isset($id)) show_404();
        $materialcosts = $this->MaterialCosts_Model;
        if ($materialcosts->delete($id)) {
            $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">
            Data Berhasil Di Hapus!</div>');
            redirect('MaterialCosts');
        }
    }

    // export to excel
    public function excel()
    {
        $data['materialcosts'] = $this->MaterialCosts_Model->getAll();
        include APPPATH.'PHPExcel-1.8/Classes/PHPExcel.php';
        include APPPATH.'PHPExcel-1.8/Classes/PHPExcel/Writer/Excel2007.php';

        $object = new PHPExcel();
        $object->getProperties()->setCreator("Biaya Bahan Baku Utilitas");
        $object->getProperties()->setLastModifiedBy("Biaya Bahan Baku Utilitas");
        $object->getProperties()->setTitle("Biaya Detail Bahan Baku Utilitas");

        $object->setActiveSheetIndex(0);

        $object->getActiveSheet()->setCellValue('A1', 'NO');
        $object->getActiveSheet()->setCellValue('B1', 'Deskripsi');
        $object->getActiveSheet()->setCellValue('C1', 'Total');
        $object->getActiveSheet()->setCellValue('D1', 'Harga Satuan');
        $object->getActiveSheet()->setCellValue('E1', 'Biaya per Tahun');

        $baris = 2;
        $no = 1;

        foreach ($data['materialcosts'] as $mc) {
            $object->getActiveSheet()->setCellValue('A'.$baris, $no++);
            $object->getActiveSheet()->setCellValue('B'.$baris, $mc['title']);
            $object->getActiveSheet()->setCellValue('C'.$baris, $mc['total']);
            $object->getActiveSheet()->setCellValue('D'.$baris, 'Rp.' . number_format($mc['price_unit']));
            $object->getActiveSheet()->setCellValue('E'.$baris, 'Rp.' . number_format($mc['costs_year']));

            $baris++;
        }

        $filename = "Data_Biaya_Bahan_Baku".'.xlsx';

        $object->getActiveSheet()->setTitle("Data Biaya Bahan Baku Utilitas");

        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
		header('Content-Disposition: attachment;filename="'. $filename .'"'); 
		header('Cache-Control: max-age=0');

        $writer = PHPExcel_IOFactory::createWriter($object, 'Excel2007');
        $writer->save('php://output');

        exit;

    }
}

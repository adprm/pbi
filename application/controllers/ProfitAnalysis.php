<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class ProfitAnalysis extends CI_Controller {

	// consturct
    public function __construct()
    {
        parent::__construct();
        is_logged_in();
        // load model
        $this->load->model('ProfitAnalysis_Model');
        $this->load->model('Configuration_Model');
        $this->load->model('Global_Model');
	}

    // function index profitanalysis
	public function index()
	{
        $data['configuration'] = $this->db->get_where('configuration', ['id' => 1])->row_array();
        $data['analisis'] = $this->Global_Model->AnalisRugiLaba();
        // nilai tahun1 - 10
        $data['analisis']['nilai1'];
        $data['analisis']['nilai2'];
        $data['analisis']['nilai3'];
        $data['analisis']['nilai4'];
        $data['analisis']['nilai2'];
        $data['analisis']['nilai5'];
        $data['analisis']['nilai6'];
        $data['analisis']['nilai7'];
        $data['analisis']['nilai8'];
        $data['analisis']['nilai9'];
        $data['analisis']['nilai10'];
        // biaya tetap
        $data['analisis']['totalbiayatetap'];
        $data['analisis']['bt1'];
        $data['analisis']['bt2'];
        $data['analisis']['bt3'];
        $data['analisis']['bt4'];
        $data['analisis']['bt2'];
        $data['analisis']['bt5'];
        $data['analisis']['bt6'];
        $data['analisis']['bt7'];
        $data['analisis']['bt8'];
        $data['analisis']['bt9'];
        $data['analisis']['bt10'];
        // biaya tidak tetap
        $data['analisis']['totalbiayatidaktetap1'];
        $data['analisis']['totalbiayatidaktetap2'];
        $data['analisis']['totalbiayatidaktetap3'];
        $data['analisis']['btt1'];
        $data['analisis']['btt2'];
        $data['analisis']['btt3'];
        $data['analisis']['btt4'];
        $data['analisis']['btt2'];
        $data['analisis']['btt5'];
        $data['analisis']['btt6'];
        $data['analisis']['btt7'];
        $data['analisis']['btt8'];
        $data['analisis']['btt9'];
        $data['analisis']['btt10'];
        // total biaya produksi
        $data['analisis']['totalBP1'];
        $data['analisis']['totalBP2'];
        $data['analisis']['totalBP3'];
        $data['analisis']['bp1'];
        $data['analisis']['bp2'];
        $data['analisis']['bp3'];
        $data['analisis']['bp4'];
        $data['analisis']['bp2'];
        $data['analisis']['bp5'];
        $data['analisis']['bp6'];
        $data['analisis']['bp7'];
        $data['analisis']['bp8'];
        $data['analisis']['bp9'];
        $data['analisis']['bp10'];
        // laba operasi new
        $data['lo1'] = $data['analisis']['nilai1'] - $data['analisis']['totalBP1'];
        $data['lo2'] = $data['analisis']['nilai2'] - $data['analisis']['totalBP2'];
        $data['lo3'] = $data['analisis']['nilai3'] - $data['analisis']['totalBP3'];
        $data['analisis']['laba1'];
        $data['analisis']['laba2'];
        $data['analisis']['laba3'];
        $data['analisis']['laba4'];
        $data['analisis']['laba2'];
        $data['analisis']['laba5'];
        $data['analisis']['laba6'];
        $data['analisis']['laba7'];
        $data['analisis']['laba8'];
        $data['analisis']['laba9'];
        $data['analisis']['laba10'];
        // penyusutan
        $data['analisis']['penyusutan1'];
        // laba sebelum pajak
        $data['lsp1'] = $data['lo1'] - $data['analisis']['penyusutan1'];
        $data['lsp2'] = $data['lo2'] - $data['analisis']['penyusutan1'];
        $data['lsp3'] = $data['lo3'] - $data['analisis']['penyusutan1'];
        $data['analisis']['lsp1'];
        $data['analisis']['lsp2'];
        $data['analisis']['lsp3'];
        $data['analisis']['lsp4'];
        $data['analisis']['lsp2'];
        $data['analisis']['lsp5'];
        $data['analisis']['lsp6'];
        $data['analisis']['lsp7'];
        $data['analisis']['lsp8'];
        $data['analisis']['lsp9'];
        $data['analisis']['lsp10'];
        // pajak
        $data['pajak1'] = $data['lsp1'] * $data['configuration']['percent_pajak'] / 100;
        $data['pajak2'] = $data['lsp2'] * $data['configuration']['percent_pajak'] / 100;
        $data['pajak3'] = $data['lsp3'] * $data['configuration']['percent_pajak'] / 100;
        
        $data['analisis']['pajak1'];
        $data['analisis']['pajak2'];
        $data['analisis']['pajak3'];
        $data['analisis']['pajak4'];
        $data['analisis']['pajak2'];
        $data['analisis']['pajak5'];
        $data['analisis']['pajak6'];
        $data['analisis']['pajak7'];
        $data['analisis']['pajak8'];
        $data['analisis']['pajak9'];
        $data['analisis']['pajak10'];
        // laba bersih
        $data['lb1'] = $data['lsp1'] - $data['pajak1'];
        $data['lb2'] = $data['lsp2'] - $data['pajak2'];
        $data['lb3'] = $data['lsp3'] - $data['pajak3'];
        
        $data['analisis']['lb1'];
        $data['analisis']['lb2'];
        $data['analisis']['lb3'];
        $data['analisis']['lb4'];
        $data['analisis']['lb2'];
        $data['analisis']['lb5'];
        $data['analisis']['lb6'];
        $data['analisis']['lb7'];
        $data['analisis']['lb8'];
        $data['analisis']['lb9'];
        $data['analisis']['lb10'];
		// set data
		$data['title'] = 'Analisis Rugi Laba';
        $data['user'] = $this->db->get_where('user', ['email' => $this->session->userdata('email')])->row_array();
		$data['profitanalysis'] = $this->ProfitAnalysis_Model->getAll();
		$data['investmentcosts'] = $this->db->get('investment_costs')->result_array();
		$data['category'] = $this->db->get_where('category', ['type' => 'analisa_rugi_laba'])->result_array();
		$query = 'SELECT * FROM sub_category WHERE category_id IN (48, 49)';
		$data['subcategory'] = $this->db->query($query)->result_array(); 

		// load view
		$this->load->view('templates/overview_header', $data);
		$this->load->view('templates/overview_sidebar');
		$this->load->view('templates/overview_topbar', $data);
		$this->load->view('profitanalysis/index', $data);
		$this->load->view('templates/overview_footer');
	}

	// function add profit analysis
    public function add()
    {
		$this->ProfitAnalysis_Model->save();
		$this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">
		Berhasil Menambahkan Data!</div>');
		redirect('ProfitAnalysis');
        
	}

    // function edit profit analysis
    public function edit($id = null)
    {
         // set data
        $data['title'] = "Ubah Nilai Produksi";
        $data['user'] = $this->db->get_where('user', ['email' => $this->session->userdata('email')])->row_array();
        $profitanalysis = $this->ProfitAnalysis_Model;
        $data['profitanalysis'] = $profitanalysis->getById($id);
        
        // validation id
        if (!isset($id)) redirect('ProfitAnalysis');
        if (!$data['profitanalysis']) show_404();
        // validation
        $this->form_validation->set_rules('category_id', 'UE (th)', 'required', [
            'required' => 'Kategori harus di isi!'
        ]);
        if ($this->form_validation->run() == false) {
            // load view
            $this->load->view('templates/overview_header', $data);
            $this->load->view('templates/overview_sidebar');
            $this->load->view('templates/overview_topbar', $data);
            $this->load->view('profitanalysis/edit_profitanalysis', $data);
            $this->load->view('templates/overview_footer');
        } else {
            // var post
            $post = $this->input->post();
            // set time zone
            date_default_timezone_set("Asia/Jakarta");
            // data array
            $data = [
                'id' => $post['id'],
                'category_id' => $post['category_id'],
                'sub_category_id' => $post['sub_category_id'],
                '1st_year' => $post['1st_year'],
                '2nd_year' => $post['2nd_year'],
                '3rd_year' => $post['3rd_year'],
                '4rd_year' => $post['4rd_year'],
                'created_at' => $post['created_at'],
                'updated_at' => date("y-m-d h:i:sa"),
                '5rd_year' => $post['5rd_year'],
                '6rd_year' => $post['6rd_year'],
                '7rd_year' => $post['7rd_year'],
                '8rd_year' => $post['8rd_year'],
                '9rd_year' => $post['9rd_year'],
                '10rd_year' => $post['10rd_year'],
            ];
            // var id
            $id = $post['id'];
            // call model
            $profitanalysis->update($data, $id);
            // set session
            $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">
            Data Berhasil Diubah!</div>');
            // redirect
            redirect('ProfitAnalysis');
        }
    }
    
    
    // edit nilai penerimaan
    public function editNilaiPenerimaan($id = null)
    {
         // set data
        $data['title'] = "Ubah Nilai Penerimaan";
        $data['user'] = $this->db->get_where('user', ['email' => $this->session->userdata('email')])->row_array();
        $configuration = $this->Configuration_Model;
        $data['configuration'] = $configuration->getById($id);
        
        // validation id
        if (!isset($id)) redirect('ProfitAnalysis');
        if (!$data['configuration']) show_404();
        // validation
        $this->form_validation->set_rules('acceptance_value', 'Nilai Penerimaan', 'required', [
            'required' => 'Nilai penerimaan harus di isi!'
        ]);
        if ($this->form_validation->run() == false) {
            // load view
            $this->load->view('templates/overview_header', $data);
            $this->load->view('templates/overview_sidebar');
            $this->load->view('templates/overview_topbar', $data);
            $this->load->view('profitanalysis/edit_acceptance_value', $data);
            $this->load->view('templates/overview_footer');
        } else {
            // var post
            $post = $this->input->post();
            // set time zone
            date_default_timezone_set("Asia/Jakarta");
            // data array
            $data = [
                'id' => $post['id'],
                'interest_loan_costs' => $post['interest_loan_costs'],
                'acceptance_value' => $post['acceptance_value'],
                'percent_pajak' => $post['percent_pajak'],
            ];
            // var id
            $id = $post['id'];
            // call model
            $configuration->update($data, $id);
            // set session
            $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">
            Data Berhasil Diubah!</div>');
            // redirect
            redirect('ProfitAnalysis');
        }
    }

    // function delete profit analysis
    public function delete($id = null)
    {
        if (!isset($id)) show_404();
        $profitanalysis = $this->ProfitAnalysis_Model;
        if ($profitanalysis->delete($id)) {
            $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">
            Data Berhasil Di Hapus!</div>');
            redirect('ProfitAnalysis');
        }
    }
	
	// export to excel
    public function excel()
    {
        $data['profitanalysis'] = $this->ProfitAnalysis_Model->getAll();
        include APPPATH.'PHPExcel-1.8/Classes/PHPExcel.php';
        include APPPATH.'PHPExcel-1.8/Classes/PHPExcel/Writer/Excel2007.php';

        $object = new PHPExcel();
        $object->getProperties()->setCreator("Analisis Rugi Laba");
        $object->getProperties()->setLastModifiedBy("Analisis Rugi Laba");
        $object->getProperties()->setTitle("Analisis Rugi Laba");

        $object->setActiveSheetIndex(0);

        $object->getActiveSheet()->setCellValue('A1', 'NO');
        $object->getActiveSheet()->setCellValue('B1', 'Kategori');
        $object->getActiveSheet()->setCellValue('C1', 'Sub Kategori');
        $object->getActiveSheet()->setCellValue('D1', 'Tahun 1');
        $object->getActiveSheet()->setCellValue('E1', 'Tahun 2');
        $object->getActiveSheet()->setCellValue('F1', 'Tahun 3-5');
        $object->getActiveSheet()->setCellValue('G1', 'Tahun 6 - 10');

        $baris = 2;
        $no = 1;

        foreach ($data['profitanalysis'] as $icd) {
            $object->getActiveSheet()->setCellValue('A'.$baris, $no++);
            $object->getActiveSheet()->setCellValue('B'.$baris, $icd['title_c']);
            $object->getActiveSheet()->setCellValue('C'.$baris, $icd['title_sc']);
            $object->getActiveSheet()->setCellValue('D'.$baris, $icd['1st_year']);
            $object->getActiveSheet()->setCellValue('E'.$baris, $icd['2nd_year']);
            $object->getActiveSheet()->setCellValue('F'.$baris, $icd['3_5rd_year']);
            $object->getActiveSheet()->setCellValue('G'.$baris, $icd['6_10rd_year']);

            $baris++;
        }

        $filename = "Analisis_Rugi_Laba".'.xlsx';

        $object->getActiveSheet()->setTitle("Analisis Rugi Laba");

        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
		header('Content-Disposition: attachment;filename="'. $filename .'"'); 
		header('Cache-Control: max-age=0');

        $writer = PHPExcel_IOFactory::createWriter($object, 'Excel2007');
        $writer->save('php://output');

        exit;

    }
}

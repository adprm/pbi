<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User extends CI_Controller {

    // construct
    public function __construct()
    {
        parent::__construct();
        is_logged_in();
        // load model
        $this->load->model('User_Model');
    }

    // function index
    public function index()
    {
        // set data
        $data['user'] = $this->db->get_where('user', ['email' => $this->session->userdata('email')])->row_array();
        $data['title'] = 'Profile';
        // load view
		$this->load->view('templates/overview_header', $data);
		$this->load->view('templates/overview_sidebar');
		$this->load->view('templates/overview_topbar', $data);
		$this->load->view('user/index', $data);
		$this->load->view('templates/overview_footer');
    }
    
    // function edit
    public function edit()
    {
        // set data
        $data['title'] = 'Edit Profile';
        $data['user'] = $this->db->get_where('user', ['email' => $this->session->userdata('email')])->row_array();
        // validation
        $this->form_validation->set_rules('name', 'Nama lengkap', 'required', [
            'required' => 'Nama lengkap tidak boleh kosong!'
        ]);

        if ($this->form_validation->run() == false) {
            // load view
            $this->load->view('templates/overview_header', $data);
            $this->load->view('templates/overview_sidebar');
            $this->load->view('templates/overview_topbar');
            $this->load->view('user/edit', $data);
            $this->load->view('templates/overview_footer');
        } else {
            $name = $this->input->post('name');
            $email = $this->input->post('email');

            // cek gambar yang akan di upload
            $upload_image  = $_FILES['image']['name'];

            if ($upload_image) {
                $config['allowed_types']    = 'jpg|jpeg|png';
                $config['max_size']         = '6000';
                $config['upload_path']      = './assets/img/profile/';

                $this->load->library('upload', $config);

                if ($this->upload->do_upload('image')) {
                    $old_image = $data['user']['image'];
                    if ($old_image != 'default.jpg') {
                        unlink(FCPATH . 'assets/img/profile/' . $old_image);
                    }

                    $new_image = $this->upload->data('file_name');
                    $this->db->set('image', $new_image);
                } else {
                    echo $this->upload->display_errors();
                }
            }


            $this->db->set('name', $name);
            $this->db->where('email', $email);
            $this->db->update('user');
            // set flash data success
            $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">
            Profile anda berhasil di ubah!</div>');
            redirect('User');
        }
    }

    public function changepassword() {
        $data['title'] = 'Change Password';
        $data['user'] = $this->db->get_where('user', ['email' => $this->session->userdata('email')])->row_array();

        $this->form_validation->set_rules('current_password', 'Current Password', 'required|trim');
        $this->form_validation->set_rules('new_password1', 'New Password', 'required|trim|min_length[6]|matches[new_password2]');
        $this->form_validation->set_rules('new_password2', 'Confirm New Password', 'required|trim|min_length[6]|matches[new_password1]');

        if ($this->form_validation->run() == false) {
            // load view
            $this->load->view('templates/overview_header', $data);
            $this->load->view('templates/overview_sidebar');
            $this->load->view('templates/overview_topbar');
            $this->load->view('user/changepassword', $data);
            $this->load->view('templates/overview_footer');
        } else {
            $current_password = $this->input->post('current_password');
            $new_password = $this->input->post('new_password1');
            // cek sama atau tidak password input dengan password yang ada di db
            if (!password_verify($current_password, $data['user']['password'])) {
                $this->session->set_flashdata('message_error_inputcurrentpassword', '<div class="alert alert-danger" role="alert">
                Kata sandi lama anda salah!</div>');
                redirect('User/changepassword');
            } else {
                if ($current_password == $new_password) {
                    $this->session->set_flashdata('message_error_inputnewpassword', '<div class="alert alert-danger" role="alert">
                    kata sandi baru tidak boleh sama dengan kata sandi lama!</div>');
                    redirect('User/changepassword');
                } else {
                    // password sudah ok
                    $password_hash = password_hash($new_password, PASSWORD_DEFAULT);
                    
                    $this->db->set('password', $password_hash);
                    $this->db->where('email', $this->session->userdata('email'));
                    $this->db->update('user');

                    $this->session->set_flashdata('message_success_changepassword', '<div class="alert alert-success" role="alert">
                    kata sanda berhasil diubah!</div>');
                    redirect('User/changepassword');
                }
            }
        }
        
    }

    public function adduser()
    {
        // validation
        $this->form_validation->set_rules('name', 'Name', 'required|trim', [
            'required' => 'Nama tidak boleh kosong!'
        ]);
        $this->form_validation->set_rules('email', 'Email', 'required|trim|valid_email|is_unique[user.email]', [
            'required' => 'Email tidak boleh kosong!',
            'valid_email' => 'Email tidak valid!',
            'is_unique' => 'Email sudah teregistrasi!'
        ]);
        $this->form_validation->set_rules('password1', 'Password', 'required|trim|min_length[5]|matches[password2]', [
            'required' => 'Password tidak boleh kosong!',
            'matches' => 'Kata sandi tidak sama!',
            'min_length' => 'Kata sandi terlalu pendek!'
        ]);
        $this->form_validation->set_rules('password2', 'Password', 'required|trim|min_length[5]|matches[password1]');
        $this->form_validation->set_rules('role', 'Tipe user', 'required', [
            'requied' => 'Pilih tipe user'
        ]);

        // load view
        if ($this->form_validation->run() === false) {
             // set data
            $data['title'] = 'Tambah Pengguna';
            $data['user'] = $this->db->get_where('user', ['email' => $this->session->userdata('email')])->row_array();
            $data['alluser'] = $this->User_Model->getAll();
            $data['userrole'] = $this->db->get('user_role')->result_array();
            // load view
            $this->load->view('templates/overview_header', $data);
            $this->load->view('templates/overview_sidebar');
            $this->load->view('templates/overview_topbar', $data);
            $this->load->view('user/adduser', $data);
            $this->load->view('templates/overview_footer');
        } else {
            $data = [
                'name' => htmlspecialchars($this->input->post('name', true)),
                'email' => htmlspecialchars($this->input->post('email', true)),
                'image' => 'default.jpg',
                'password' => password_hash($this->input->post('password1'), PASSWORD_DEFAULT),
                'is_active' => 1,
                'date_created' => time(),
                'role_id' => $this->input->post('role')
            ];
            $this->db->insert('user', $data);
            $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">
            Akun berhasil dibuat</div>');
            redirect('User/adduser');
        }

    }

    // delete account
    public function delete($id)
    {
        $this->db->delete('user', ['id' => $id]);
        $this->session->unset_userdata('email');
        $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">
        Akun berhasil dihapus!</div>');
        redirect('Auth');
    }

    // delete user
    public function deleteuser($id = null)
    {
        if (!isset($id)) show_404();
        $user = $this->User_Model;
        if ($user->delete($id)) {
            $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">
            Data Berhasil Di Hapus!</div>');
            redirect('User/adduser');
        }
    }

}
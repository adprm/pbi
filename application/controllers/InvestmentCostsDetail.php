<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class InvestmentCostsDetail extends CI_Controller {

    // consturct
    public function __construct()
    {
        parent::__construct();
        is_logged_in();
        // load model
        $this->load->model('Global_Model');
        $this->load->model('InvestmentCostsDetail_Model');
        $this->load->model('SubCategory_Model');
    }

    // index investmentcostsdetail
    public function index()
    {
        // sub total pengadaan lahan
        $data['stpengadaan_lahan'] = $this->Global_Model->pengadaanLahan();
        // sub total bangunan
        $data['stbangunan'] = $this->Global_Model->bangunan();
        // sub total mesin dan peralatan
        $data['stmesin'] = $this->Global_Model->mesinPeralatan();
        // sub total fasilitsa
        $data['stfasilitas'] = $this->Global_Model->fasilitas();
        // sub total kendaraan
        $data['stkendaraan'] = $this->Global_Model->kendaraan();
        // sub total pra investasi
        $data['stprainvest'] = $this->Global_Model->prainvestasi();
        // total investasi
        $data['totalinvestasi'] = $this->Global_Model->totalInvestasi($data['stpengadaan_lahan'], $data['stbangunan'], $data['stmesin'], $data['stfasilitas'], $data['stkendaraan'], $data['stprainvest']);
        // get total modal kerja
        $data['modalkerja'] = $this->Global_Model->modalKerja();
        // total kotingensi
        $data['kotingensi'] = $this->Global_Model->kotingensi($data['totalinvestasi'], $data['modalkerja']['subtotal']);
        // total biaya investasi
        $data['totalbaiayainvest'] = $this->Global_Model->totalBiayaInvest($data['totalinvestasi'], $data['modalkerja']['subtotal'], $data['kotingensi']);

        // set data
        $data['title'] = 'Biaya Investasi';
        $data['user'] = $this->db->get_where('user', ['email' => $this->session->userdata('email')])->row_array();
        $data['investmentcostsdetail'] = $this->InvestmentCostsDetail_Model->getAll();
        $data['category'] = $this->db->get_where('category', ['type' => 'biaya_investasi'])->result_array();
        $data['subcategory'] = $this->InvestmentCostsDetail_Model->getSubCategory();
        $data['unit'] = $this->InvestmentCostsDetail_Model->getUnit();
        // load view
		$this->load->view('templates/overview_header', $data);
		$this->load->view('templates/overview_sidebar');
		$this->load->view('templates/overview_topbar', $data);
		$this->load->view('investmentcostsdetail/index', $data);
		$this->load->view('templates/overview_footer');
    }

    // function list sub category
    public function listSubCategory()
    {
        $id_category = $this->input->post('id_category');
        $subcategory = $this->SubCategory_Model->ViewByCategory($id_category);

        $lists = "<option value=''>Pilih Sub Kategori</option>";

        foreach($subcategory as $data) {
            $lists .= "<option value='".$data['id']."'>".$data['title']."</option>";
        }

        $callback = array('list_sub_category' => $lists);

        echo json_encode($callback);
    }

    // add investmentcostsdetail
    public function add()
    {
        // validation
        $this->form_validation->set_rules('category_id', 'Kategori', 'required', [
            'required' => 'Kategori harus di isi!'
        ]);

        if ($this->form_validation->run() == false) {
            // load view
            $this->load->view('templates/overview_header', $data);
            $this->load->view('templates/overview_sidebar');
            $this->load->view('templates/overview_topbar');
            $this->load->view('investmentcostsdetail/index', $data);
            $this->load->view('templates/overview_footer');
            $this->session->set_flashdata('message', '<div class="alert alert-danger" role="alert">
            Gagal Menambahkan Data!</div>');
            redirect('InvestmentCostsDetail');
        } else {
            // hitung sub total
            $st = 'SELECT SUM(subtotal) AS total FROM investment_costs_detail WHERE category_id != 0';
            $subt = $this->db->query($st)->row()->total;
            // var post
            $post = $this->input->post();
            // logic sub total
            if ($post['total'] == 0) {
                $subtotal = $this->subtotal = $post['total'] + $post['price_per_unit'];
            } else {
                $subtotal = $this->subtotal = $post['total'] * $post['price_per_unit'];
            }
            // array data
            $data = [
                'category_id' => $post['category_id'],
                'sub_category_id' => $post['sub_category_id'],
                'unit_id' => $post['unit_id'],
                'total' => $post['total'],
                'price_per_unit' => $post['price_per_unit'],
                'subtotal' => $subtotal
            ];
            // call method save
            $this->InvestmentCostsDetail_Model->save($data);
            // set session
            $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">
            Berhasil Menambahkan Data!</div>');
            // redirect
            redirect('InvestmentCostsDetail');
        }
    }

    // edit investmentcostsdetail
    public function edit($id = null)
    {
        // var model
        $investmentcostsdetail = $this->InvestmentCostsDetail_Model;
        // set data
        $data['title'] = "Ubah Biaya Investasi";
        $data['user'] = $this->db->get_where('user', ['email' => $this->session->userdata('email')])->row_array();
        $data['investmentcostsdetail'] = $investmentcostsdetail->getById($id);
        $data['category'] = $this->db->get_where('category', ['type' => 'biaya_investasi'])->result_array();
        $data['subcategory'] = $investmentcostsdetail->getSubCategory();
        $data['unit'] = $investmentcostsdetail->getUnit();
        // validation id
        if (!isset($id)) redirect('InvestmentCostsDetail');
        if (!$data['investmentcostsdetail']) show_404();

        // validation
        $this->form_validation->set_rules('category_id', 'Kategori', 'required', [
            'required' => 'Kategori harus di isi!'
        ]);
        $this->form_validation->set_rules('sub_category_id', 'Sub Kategori', 'required', [
            'required' => 'Sub kategori harus di isi!'
        ]);
        $this->form_validation->set_rules('price_per_unit', 'Harga', 'required', [
            'required' => 'Harga per unit harus di isi!'
        ]);

        if ($this->form_validation->run() == false) {
            // load view
            $this->load->view('templates/overview_header', $data);
            $this->load->view('templates/overview_sidebar');
            $this->load->view('templates/overview_topbar');
            $this->load->view('investmentcostsdetail/edit_investmentcostsdetail', $data);
            $this->load->view('templates/overview_footer');
            // set session
            $this->session->set_flashdata('message', '<div class="alert alert-danger" role="alert">
            Data Gagal Diubah!</div>');
        } else {
            // hitung sub total
            $st = 'SELECT SUM(subtotal) AS total FROM investment_costs_detail WHERE category_id != 0';
            $subt = $this->db->query($st)->row()->total;
            // var post
            $post = $this->input->post();
            // logic sub total
            if ($post['total'] == 0) {
                $subtotal = $this->subtotal = $post['total'] + $post['price_per_unit'];
            } else {
                $subtotal = $this->subtotal = $post['total'] * $post['price_per_unit'];
            }
            // array data
            $data = [
                'category_id' => $post['category_id'],
                'sub_category_id' => $post['sub_category_id'],
                'unit_id' => $post['unit_id'],
                'total' => $post['total'],
                'price_per_unit' => $post['price_per_unit'],
                'subtotal' => $subtotal
            ];
            // var id
            $id = $post['id'];
            // call method update
            $investmentcostsdetail->update($data, $id);
            // set session
            $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">
            Data Berhasil Diubah!</div>');
            // redirect
            redirect('InvestmentCostsDetail');
        }
    }

    // edit investmentcostsdetail
    public function editmodalkerja($id = null)
    {
        // var model
        $investmentcostsdetail = $this->InvestmentCostsDetail_Model;
        // set data
        $data['title'] = "Ubah Biaya Investasi";
        $data['user'] = $this->db->get_where('user', ['email' => $this->session->userdata('email')])->row_array();
        $data['investmentcostsdetail'] = $investmentcostsdetail->getById($id);
        // validation id
        if (!isset($id)) redirect('InvestmentCostsDetail');
        if (!$data['investmentcostsdetail']) show_404();

        // validation
        
        $this->form_validation->set_rules('price_per_unit', 'Modal kerja', 'required', [
            'required' => 'Modal kerja harus di isi!'
        ]);

        if ($this->form_validation->run() == false) {
            // load view
            $this->load->view('templates/overview_header', $data);
            $this->load->view('templates/overview_sidebar');
            $this->load->view('templates/overview_topbar');
            $this->load->view('investmentcostsdetail/editmodalkerja', $data);
            $this->load->view('templates/overview_footer');
            // set session
            $this->session->set_flashdata('message', '<div class="alert alert-danger" role="alert">
            Data Gagal Diubah!</div>');
        } else {            
            // array data
            $post = $this->input->post();
            $data = [
                'category_id' => $post['category_id'],
                'sub_category_id' => $post['sub_category_id'],
                'unit_id' => $post['unit_id'],
                'total' => $post['total'],
                'price_per_unit' => $post['price_per_unit'],
                'subtotal' => $post['price_per_unit']
            ];
            // var id
            $id = $post['id'];
            var_dump($data);
            // call method update
            $investmentcostsdetail->update($data, $id);
            // set session
            $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">
            Data Berhasil Diubah!</div>');
            // redirect
            redirect('InvestmentCostsDetail');
        }
    }

    // delete investmentcosts
    public function delete($id = null)
    {
        if (!isset($id)) show_404();
        $investmentcostsdetail = $this->InvestmentCostsDetail_Model;
        if ($investmentcostsdetail->delete($id)) {
            redirect('InvestmentCostsDetail');
        }
    }

    // export to excel
    public function excel()
    {
        $data['investmentcostsdetail'] = $this->InvestmentCostsDetail_Model->getAll();
        include APPPATH.'PHPExcel-1.8/Classes/PHPExcel.php';
        include APPPATH.'PHPExcel-1.8/Classes/PHPExcel/Writer/Excel2007.php';

        $object = new PHPExcel();
        $object->getProperties()->setCreator("Biaya Investasi");
        $object->getProperties()->setLastModifiedBy("Biaya Investasi");
        $object->getProperties()->setTitle("Biaya Detail Investasi");

        $object->setActiveSheetIndex(0);

        $object->getActiveSheet()->setCellValue('A1', 'NO');
        $object->getActiveSheet()->setCellValue('B1', 'Kategori');
        $object->getActiveSheet()->setCellValue('C1', 'Sub Kategori');
        $object->getActiveSheet()->setCellValue('D1', 'Unit');
        $object->getActiveSheet()->setCellValue('E1', 'Total');
        $object->getActiveSheet()->setCellValue('F1', 'Harga Satuan (RP)');
        $object->getActiveSheet()->setCellValue('G1', 'Total Biaya');

        $baris = 2;
        $no = 1;

        foreach ($data['investmentcostsdetail'] as $icd) {
            $object->getActiveSheet()->setCellValue('A'.$baris, $no++);
            $object->getActiveSheet()->setCellValue('B'.$baris, $icd['title_c']);
            $object->getActiveSheet()->setCellValue('C'.$baris, $icd['title_sc']);
            $object->getActiveSheet()->setCellValue('D'.$baris, $icd['title_u']);
            $object->getActiveSheet()->setCellValue('E'.$baris, $icd['total']);
            $object->getActiveSheet()->setCellValue('F'.$baris, number_format($icd['price_per_unit']));
            $object->getActiveSheet()->setCellValue('G'.$baris, number_format($icd['subtotal']));

            $baris++;
        }

        $filename = "Data_Biaya_Investasi".'.xlsx';

        $object->getActiveSheet()->setTitle("Data Biaya Investasi");

        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
		header('Content-Disposition: attachment;filename="'. $filename .'"'); 
		header('Cache-Control: max-age=0');

        $writer = PHPExcel_IOFactory::createWriter($object, 'Excel2007');
        $writer->save('php://output');

        exit;

    }
}
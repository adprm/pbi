<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class SubCategory extends CI_Controller {

    // consturct
    public function __construct()
    {
        parent::__construct();
        is_logged_in();
        // load model
        $this->load->model('SubCategory_Model');
    }

    // index subcategory
	public function index()
	{
		// set data
        $data['title'] = 'Sub Kategori';
        $data['user'] = $this->db->get_where('user', ['email' => $this->session->userdata('email')])->row_array();
        $data['subcategory'] = $this->SubCategory_Model->getAll();
        $data['category'] = $this->db->get('category')->result_array();
        // load view
		$this->load->view('templates/overview_header', $data);
		$this->load->view('templates/overview_sidebar');
		$this->load->view('templates/overview_topbar', $data);
		$this->load->view('sub_category/index', $data);
		$this->load->view('templates/overview_footer');
    }
    
    // add sucategory
    public function add()
    {
        // validation
        $this->form_validation->set_rules('category_id', 'Kategori', 'required', [
            'required' => 'Kategori harus di isi!'
        ]);
        $this->form_validation->set_rules('title', 'Nama Sub Kategori', 'required', [
            'required' => 'Nama sub kategori harus di isi!'
        ]);

        if ($this->form_validation->run() == false) {
            // load view
            $this->load->view('templates/overview_header', $data);
            $this->load->view('templates/overview_sidebar');
            $this->load->view('templates/overview_topbar');
            $this->load->view('sub_category/index', $data);
            $this->load->view('templates/overview_footer');
            $this->session->set_flashdata('message', '<div class="alert alert-danger" role="alert">
            Gagal Menambahkan Data!</div>');
            redirect('SubCategory');
        } else {
            $this->SubCategory_Model->save();
            $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">
            Berhasil Menambahkan Data!</div>');
            redirect('SubCategory');
        }
    }

    // edit category
    public function edit($id = null)
    {
        // set data
        $data['title'] = "Ubah Sub Kategori";
        $data['user'] = $this->db->get_where('user', ['email' => $this->session->userdata('email')])->row_array();
        $subcategory = $this->SubCategory_Model;
        $data['subcategory'] = $subcategory->getById($id);
        $data['category'] = $this->db->get('category')->result_array();
        // validation id
        if (!isset($id)) redirect('SubCategory');
        if (!$data['subcategory']) show_404();
        // validation
        $this->form_validation->set_rules('category_id', 'Kategori', 'required', [
            'required' => 'Kategori harus di isi!'
        ]);
        $this->form_validation->set_rules('title', 'Nama Sub Kategori', 'required', [
            'required' => 'Nama sub kategori harus di isi!'
        ]);

        if ($this->form_validation->run() == false) {
            // load view
            $this->load->view('templates/overview_header', $data);
            $this->load->view('templates/overview_sidebar');
            $this->load->view('templates/overview_topbar');
            $this->load->view('sub_category/edit_subcategory', $data);
            $this->load->view('templates/overview_footer');
            $this->session->set_flashdata('message', '<div class="alert alert-danger" role="alert">
            Data Gagal Diubah!</div>');
        } else {
            $subcategory->update();
            $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">
            Data Berhasil Diubah!</div>');
            redirect('SubCategory');
        }
    }

    // delete category
    public function delete($id = null)
    {
        if (!isset($id)) show_404();
        $subcategory = $this->SubCategory_Model;
        if ($subcategory->delete($id)) {
            $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">
            Data Berhasil Di Hapus!</div>');
            redirect('SubCategory');
        }
    }
}

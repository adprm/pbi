<!-- Begin Page Content -->
<div class="container-fluid">
    <!-- Page Heading -->
    <h1 class="h3 mb-4 text-gray-800"><?= $title; ?></h1>
    <div class="col-lg-7">
        <?= form_error('title', '<div class="alert alert-danger" role="alert">', '</div>'); ?>
        <?= $this->session->flashdata('message'); ?>
    </div>

    <div class="card col-lg-12 shadow mb-4">
        <div class="card-header py-3">
            <h6 class="m-0 font-weight-bold text-primary">Analisis Finansial</h6>
        </div>
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                        <thead>
                                <tr>
                                    <th>Deskripsi</th>
                                    <th>Tahun 1</th> 
                                    <th>Tahun 2</th> 
                                    <th>Tahun 3 s.d 10</th> 
                                </tr>
                        </thead>
                    <tbody>
                        <tr>
                            <td>ROI</td>
                            <td><?= number_format((float)$roi1, 2); ?></td>
                            <td><?= number_format((float)$roi1, 2); ?></td>
                            <td><?= number_format((float)$roi3, 2); ?></td>
                        </tr>
                        <tr>
                            <td>PBP</td>
                            <td><?= number_format((float)$total_pbp1, 2); ?></td>
                            <td><?= number_format((float)$total_pbp2, 2); ?></td>
                            <td><?= number_format((float)$total_pbp3, 2); ?></td>
                        </tr>
                        <tr>
                            <td>BEP (Rupiah)</td>
                            <td><?= number_format($total_bep1); ?></td>
                            <td><?= number_format($total_bep2); ?></td>
                            <td><?= number_format($total_bep3); ?></td>
                        </tr>
                        <tr>
                            <td>BEP (Unit)</td>
                            <td><?= number_format($bepunit1); ?></td>
                            <td><?= number_format($bepunit2); ?></td>
                            <td><?= number_format($bepunit3); ?></td>
                        </tr>
                        <tr>
                            <td>NPV</td>
                            <td><?= number_format((float)$npv1, 2); ?></td>
                            <td><?= number_format((float)$npv2, 2); ?></td>
                            <td><?= number_format((float)$npv3, 2); ?></td>
                        </tr>
                    </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

<!-- /.container-fluid -->
</div>
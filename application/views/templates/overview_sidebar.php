<!-- Sidebar -->
    <ul class="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion" id="accordionSidebar">

      <!-- Sidebar - Brand -->
      <a class="sidebar-brand d-flex align-items-center justify-content-center mt-2" href="<?= base_url('Overview'); ?>">       
          <small><?= SITE_NAME ?></small>
      </a>

      <!-- Divider -->
      <hr class="sidebar-divider my-0 mt-3">

      <!-- Nav Item - Dashboard -->
      <li class="nav-item <?php if ($this->uri->segment(1) == "Overview") {echo "active";} ?>">
        <a class="nav-link" href="<?= base_url('Overview'); ?>">
          <i class="fas fa-fw fa-tachometer-alt"></i>
          <span>Dashboard</span></a>
      </li>

      <!-- Divider user data -->
      <hr class="sidebar-divider">

      <!-- Heading -->
      <div class="sidebar-heading">
        User Manajemen
      </div>

      <!-- Nav Item - Pages Collapse Menu -->
      <li class="nav-item <?php 
      if ($this->uri->segment(1) == "User") {
        echo "active"; 
      } ?>">
        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseuserprofile" aria-expanded="true" aria-controls="collapseuserprofile">
          <i class="fas fa-fw fa-folder"></i>
          <span>Manajemen Profil</span>
        </a>
        <div id="collapseuserprofile" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionSidebar">
          <div class="bg-white py-2 collapse-inner rounded">
            <h6 class="collapse-header">Menu</h6>
            <a class="collapse-item <?php if ($this->uri->segment(1) == "User") {echo "bg-warning";} ?>" href="<?= base_url('User'); ?>">Profil</a>
          </div>
        </div>
      </li>

      <!-- Divider master data -->
      <hr class="sidebar-divider">

      <!-- Heading -->
      <div class="sidebar-heading">
        Master Data
      </div>

      <!-- Nav Item - Pages Collapse Menu -->
      <li class="nav-item <?php 
      if ($this->uri->segment(1) == "Category") {
        echo "active";
      } else if ($this->uri->segment(1) == "SubCategory") {
        echo "active";
      } else if ($this->uri->segment(1) == "UnitData") {
        echo "active";
      }
      ?>">
        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapsemasterdata" aria-expanded="true" aria-controls="collapsemasterdata">
          <i class="fas fa-fw fa-folder"></i>
          <span>Master Data</span>
        </a>
        <div id="collapsemasterdata" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionSidebar">
          <div class="bg-white py-2 collapse-inner rounded">
            <h6 class="collapse-header">Menu</h6>
            <a class="collapse-item <?php if ($this->uri->segment(1) == "Category") {echo "bg-warning";} ?>" href="<?= base_url('Category'); ?>">Kategori</a>
            <a class="collapse-item <?php if ($this->uri->segment(1) == "SubCategory") {echo "bg-warning";} ?>" href="<?= base_url('SubCategory'); ?>">Sub Kategori</a>
            <a class="collapse-item <?php if ($this->uri->segment(1) == "UnitData") {echo "bg-warning";} ?>" href="<?= base_url('UnitData'); ?>">Unit</a>
          </div>
        </div>
      </li>
     
      <!-- Divider reduction cost -->
      <hr class="sidebar-divider">

      <!-- Nav Item -  -->
      <li class="nav-item <?php if ($this->uri->segment(1) == "InvestmentCostsDetail") {echo "active";} ?>">
        <a class="nav-link pb-3 pt-0" href="<?= base_url('InvestmentCostsDetail'); ?>">
          <i class="fas fa-fw fa-file-signature"></i>
          <span>Biaya Investasi</span></a>
      </li>
      
      <!-- Divider reduction cost -->
      <hr class="sidebar-divider">

      <!-- Nav Item -  -->
      <li class="nav-item <?php if ($this->uri->segment(1) == "ReductionCostsData") {echo "active";} ?>">
        <a class="nav-link pb-3 pt-0" href="<?= base_url('ReductionCostsData'); ?>">
          <i class="fas fa-fw fa-file-signature"></i>
          <span>Biaya Penyusutan</span></a>
      </li>

      <!-- Divider additional cost -->
      <hr class="sidebar-divider">

      <!-- Nav Item -  -->
      <li class="nav-item <?php if ($this->uri->segment(1) == "AdditionalCosts") {echo "active";} ?>">
        <a class="nav-link pb-3 pt-0" href="<?= base_url('AdditionalCosts'); ?>">
          <i class="fas fa-fw fa-file-signature"></i>
          <span>Biaya Pemeliharaan, Asuransi, Administrasi, Pajak Bumi Bangunan, Pemasaran</span></a>
      </li>

      <!-- Divider material costs -->
      <hr class="sidebar-divider">

      <!-- Nav Item -  -->
      <li class="nav-item <?php if ($this->uri->segment(1) == "MaterialCosts") {echo "active";} ?>">
        <a class="nav-link pb-3 pt-0" href="<?= base_url('MaterialCosts'); ?>">
          <i class="fas fa-fw fa-file-signature"></i>
          <span>Biaya Bahan Baku, Pembantu & Utilitas</span></a>
      </li>

      <!-- Divider employee costs -->
      <hr class="sidebar-divider">

      <!-- Nav Item -  -->
      <li class="nav-item <?php if ($this->uri->segment(1) == "EmployeeCosts") {echo "active";} ?>">
        <a class="nav-link pb-3 pt-0" href="<?= base_url('EmployeeCosts'); ?>">
          <i class="fas fa-fw fa-file-signature"></i>
          <span>Biaya Tenaga Kerja</span></a>
      </li>

      <!-- Divider production costs -->
      <hr class="sidebar-divider">

      <!-- Nav Item -  -->
      <li class="nav-item <?php if ($this->uri->segment(1) == "ProductionCosts") {echo "active";} ?>">
        <a class="nav-link pb-3 pt-0" href="<?= base_url('ProductionCosts'); ?>">
          <i class="fas fa-fw fa-file-signature"></i>
          <span>Biaya Produksi</span></a>
      </li>

      <!-- Divider loan costs -->
      <hr class="sidebar-divider">

      <!-- Nav Item -  -->
      <li class="nav-item <?php if ($this->uri->segment(1) == "LoanCosts") {echo "active";} ?>">
        <a class="nav-link pb-3 pt-0" href="<?= base_url('LoanCosts'); ?>">
          <i class="fas fa-fw fa-file-signature"></i>
          <span>Kewajiban Pinjaman</span></a>
      </li>

      <!-- Divider profit analysis -->
      <hr class="sidebar-divider">

      <!-- Nav Item -  -->
      <li class="nav-item <?php if ($this->uri->segment(1) == "ProfitAnalysis") {echo "active";} ?>">
        <a class="nav-link pb-3 pt-0" href="<?= base_url('ProfitAnalysis'); ?>">
          <i class="fas fa-fw fa-file-signature"></i>
          <span>Analisis Rugi Laba</span></a>
      </li>

      <!-- Divider cash expenses income -->
      <hr class="sidebar-divider">

      <!-- Nav Item -  -->
      <li class="nav-item <?php if ($this->uri->segment(1) == "CashExpensesIncome") {echo "active";} ?>">
        <a class="nav-link pb-3 pt-0" href="<?= base_url('CashExpensesIncome'); ?>">
          <i class="fas fa-fw fa-file-signature"></i>
          <span>Arus Kas Pemasukan dan Pengeluaran</span></a>
      </li>
      <!-- Divider cash expenses income -->
      <hr class="sidebar-divider">

      <!-- Nav Item -  -->
      <li class="nav-item <?php if ($this->uri->segment(1) == "FinancialAnalysis") {echo "active";} ?>">
        <a class="nav-link pb-3 pt-0" href="<?= base_url('FinancialAnalysis') ?>">
          <i class="fas fa-fw fa-file-signature"></i>
          <span>Analisis Finansial</span></a>
      </li>
      <!-- Divider cash expenses income -->
      <hr class="sidebar-divider">

      <!-- Nav Item -  -->
      <!--<li class="nav-item <?php if ($this->uri->segment(1) == "SensitivityAnalysis") {echo "active";} ?>">-->
      <!--  <a class="nav-link pb-3 pt-0" href="<?= base_url('SensitivityAnalysis') ?>">-->
      <!--    <i class="fas fa-fw fa-file-signature"></i>-->
      <!--    <span>Analisis Sensitifitas</span></a>-->
      <!--</li>-->
      <!-- Divider cash expenses income -->
      <!--<hr class="sidebar-divider">-->

      <!-- Nav Item -  -->
      <li class="nav-item <?php if ($this->uri->segment(1) == "Configuration") {echo "active";} ?>">
        <a class="nav-link pb-3 pt-0" href="<?= base_url('Configuration') ?>">
          <i class="fas fa-cogs"></i>
          <span>Konfigurasi</span></a>
      </li>
      <!-- Divider cash expenses income -->
      <hr class="sidebar-divider">

      <!-- Nav Item -  -->
      <li class="nav-item <?php if ($this->uri->segment(1) == "Auth/logout") {echo "active";} ?>">
        <a class="nav-link pb-3 pt-0" href="<?= base_url('Auth/logout'); ?>">
          <i class="fas fa-fw fa-sign-out-alt"></i>
          <span>Logout</span></a>
      </li>
      <!-- Divider cash expenses income -->
      <hr class="sidebar-divider">

      <!-- Sidebar Toggler (Sidebar) -->
      <div class="text-center d-none d-md-inline">
        <button class="rounded-circle border-0" id="sidebarToggle"></button>
      </div>

    </ul>
    <!-- End of Sidebar -->
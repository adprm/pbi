<script>
    function deleteConfirm(url){
        $('#btn-delete').attr('href', url);
        $('#deleteModal').modal();
    }
</script>

<!-- Begin Page Content -->
<div class="container-fluid">

    <!-- Page Heading -->
    <h1 class="h3 mb-4 text-gray-800"><?= $title; ?></h1>
    <!-- <a href="<?= base_url('AdditionalCosts/excel'); ?>" class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm mb-2"><i class="fas fa-download fa-sm text-white-50"></i> Export Data</a> -->
    <div class="col-lg-7">
        <?= form_error('title', '<div class="alert alert-danger" role="alert">', '</div>'); ?>
        <?= $this->session->flashdata('message'); ?>
    </div>

    <div class="row">
      <!-- sub total pemeliharaan -->
      <div class="col-xl-4 col-md-6 mb-4">
        <div class="card border-left-secondary shadow h-100 py-2">
          <div class="card-body">
            <div class="row no-gutters align-items-center">
              <div class="col mr-2">
                <div class="text-xs font-weight-bold text-secondary text-uppercase mb-1">Biaya Pemeliharaan</div>
                <div class="h5 mb-0 font-weight-bold text-gray-800"><?= "Rp." . number_format($stpemeliharaan); ?></div>
              </div>
              <div class="col-auto">
                <i class="fas fa-file-contract fa-2x text-gray-300"></i>
              </div>
            </div>
          </div>
        </div>
      </div>
      <!-- sub total asuransi -->
      <div class="col-xl-4 col-md-6 mb-4">
        <div class="card border-left-secondary shadow h-100 py-2">
          <div class="card-body">
            <div class="row no-gutters align-items-center">
              <div class="col mr-2">
                <div class="text-xs font-weight-bold text-secondary text-uppercase mb-1">Biaya Asuransi</div>
                <div class="h5 mb-0 font-weight-bold text-gray-800"><?= "Rp." . number_format($stasuransi); ?></div>
              </div>
              <div class="col-auto">
                <i class="fas fa-file-contract fa-2x text-gray-300"></i>
              </div>
            </div>
          </div>
        </div>
      </div>

      <!-- sub total biaya admin -->
      <div class="col-xl-4 col-md-6 mb-4">
        <div class="card border-left-success shadow h-100 py-2">
          <div class="card-body">
            <div class="row no-gutters align-items-center">
              <div class="col mr-2">
                <div class="text-xs font-weight-bold text-success text-uppercase mb-1">Biaya Administrasi</div>
                <div class="h5 mb-0 font-weight-bold text-gray-800"><?= "Rp." . number_format($biayaadministrasi['costs_year']); ?></div>
              </div>
              <div class="col-auto">
                <i class="fas fa-file-contract fa-2x text-gray-300"></i>
              </div>
            </div>
          </div>
            <div class="col-lg-4 ml-2">
              <div class="small">
                <a class="badge badge-success" href="<?= base_url('AdditionalCosts/editbiayaadmin/'.$biayaadministrasi['id']) ?>">Ubah</a>
              </div>
            </div>
          </div>
      </div>

      <!-- sub total pajak bumi dan banguanan -->
      <div class="col-xl-4 col-md-6 mb-4">
        <div class="card border-left-info shadow h-100 py-2">
          <div class="card-body">
            <div class="row no-gutters align-items-center">
              <div class="col mr-2">
                <div class="text-xs font-weight-bold text-info text-uppercase mb-1">Pajak Bumi dan Bangunan</div>
              </div>
              <div class="col-auto">
                <i class="fas fa-file-contract fa-2x text-gray-300"></i>
              </div>
            </div>
            <div class="row no-gutters align-items-center">
                <div class="col-lg-4">
                  <div class="text-xs font-weight-bold text-info text-uppercase">Biaya Investasi</div>
                </div>
                <div class="col-lg-6">
                  <div class="h5 mb-0 font-weight-bold text-gray-800"><?= "Rp." . number_format($biayapajakinvestasi['investment_costs']); ?></div>
                </div>
                <div class="col-lg-4">
                  <div class="text-xs font-weight-bold text-info text-uppercase">Biaya / Tahun</div>
                </div>
                <div class="col-lg-6">
                  <div class="h5 mb-0 font-weight-bold text-gray-800"><?= "Rp." . number_format($biayapajaktahunan); ?></div>
                </div>
            </div>
          </div>
        </div>
      </div>

      <!-- sub total pemasaran -->
      <div class="col-xl-4 col-md-6 mb-4">
        <div class="card border-left-warning shadow h-100 py-2">
          <div class="card-body">
            <div class="row no-gutters align-items-center">
              <div class="col mr-2">
                <div class="text-xs font-weight-bold text-warning text-uppercase mb-1">Biaya Pemasaran</div>
                <div class="h5 mb-0 font-weight-bold text-gray-800"><?= "Rp." . number_format($pemasaran['costs_year']); ?></div>
              </div>
              <div class="col-auto">
                <i class="fas fa-file-contract fa-2x text-gray-300"></i>
              </div>
            </div>
          </div>
            <div class="col-lg-4 ml-2">
              <div class="small">
                <a class="badge badge-warning" href="<?= base_url('AdditionalCosts/editbiayaadmin/'.$pemasaran['id']) ?>">Ubah</a>
              </div>
            </div>
          </div>
      </div>

      <!-- sub total biaya lain lain -->
      <div class="col-xl-4 col-md-6 mb-4">
        <div class="card border-left-primary shadow h-100 py-2">
          <div class="card-body">
            <div class="row no-gutters align-items-center">
              <div class="col mr-2">
                <div class="text-xs font-weight-bold text-primary text-uppercase mb-1">Total Biaya Lain-lain</div>
                <div class="h5 mb-0 font-weight-bold text-gray-800"><?= "Rp." . number_format($totalbiayalain); ?></div>
              </div>
              <div class="col-auto">
                <i class="fas fa-file-contract fa-2x text-gray-300"></i>
              </div>
            </div>
          </div>
        </div>
      </div>

    </div>

    <div class="card col-lg-12 shadow mb-4">
        <div class="card-header py-3">
            <h6 class="m-0 font-weight-bold text-primary"><a href="" data-toggle="modal" data-target="#newAdditionalCostsModal"><i class="fas fa-plus"></i> Biaya Lain-lain</a></h6>
        </div>
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table table-bordered" id="eksport" width="100%" cellspacing="0">
                        <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Ketegori</th>
                                    <th>Sub Ketegori</th>
                                    <th>Biaya Investasi</th>
                                    <th>Biaya / Tahun</th>
                                    <th>Aksi</th>
                                </tr>
                        </thead>
                    <tbody>
                        <?php $index = 1; ?>
                        <?php foreach($additionalcosts as $ac) : ?>
                        <tr>
                            <td><?= $index; ?></td>
                            <td><?= $ac['title_c']; ?></td>
                            <td><?= $ac['title_cc']; ?></td>
                            <td><?= "Rp." . number_format($ac['investment_costs']); ?></td>
                            <td><?=  "Rp." . number_format($ac['costs_year']); ?></td>
                            <td>
                                <a class="badge badge-success" href="<?= site_url('AdditionalCosts/edit/'.$ac['id']); ?>">Ubah</a>
                                <a class="badge badge-danger" href="#!" onclick="deleteConfirm('<?= site_url('AdditionalCosts/delete/'.$ac['id']); ?>')">Hapus</a>
                            </td>
                        </tr>
                        <?php $index++; ?>
                        <?php endforeach; ?>
                    </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
<!-- /.container-fluid -->
</div>
<!-- Modal add new additionalcosts -->
<div class="modal fade" id="newAdditionalCostsModal" tabindex="-1" role="dialog" aria-labelledby="newAdditionalCostsModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="newAdditionalCostsModalLabel">Tambah Biaya Lain-lain</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <!-- form -->
      <form action="<?= site_url('AdditionalCosts/add'); ?>" method="post">
        <div class="modal-body">
            <div class="form-group row">
                <div class="col-lg-6">
                  <label for="category_id">Kategori</label>
                  <select name="category_id" id="category_id" class="form-control">
                      <option value="">Pilih Kategori</option>
                      <?php foreach ($category_biayalain as $cbl) : ?>
                          <option value="<?= $cbl['id']; ?>"><?= $cbl['title']; ?></option>
                      <?php endforeach; ?>
                  </select>
                </div>
                <div class="col-lg-6">
                  <label for="sub_category_id">Sub Kategori</label>
                  <select name="sub_category_id" id="sub_category_id" class="form-control">
                    <option value="0">Pilih Sub Kategori</option>
                    <?php foreach ($category_baiayainvest as $cbi) : ?>
                      <option value="<?= $cbi['id']; ?>"><?= $cbi['title']; ?></option>
                    <?php endforeach; ?>
                  </select>
                </div>
            </div>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
            <button type="submit" class="btn btn-primary">Tambah</button>
        </div>
      </form>
    </div>
  </div>
</div>

<!-- modal delete -->
<div class="modal fade" id="deleteModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Apa Anda Yakin?</h5>
        <button class="close" type="button" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">×</span>
        </button>
      </div>
      <div class="modal-body">Data yang sudah dihapus tidak dapat dikembalikan!</div>
      <div class="modal-footer">
        <button class="btn btn-secondary" type="button" data-dismiss="modal">Batal</button>
        <a id="btn-delete" class="btn btn-danger" href="#">Hapus</a>
      </div>
    </div>
  </div>
</div>
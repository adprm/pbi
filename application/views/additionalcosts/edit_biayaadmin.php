<!-- Begin Page Content -->
<div class="container-fluid">

    <!-- Page Heading -->
    <h1 class="h3 mb-4 text-gray-800"><?= $title; ?></h1>

    <div class="card col-lg-7 shadow mb-4">
        <div class="card-header py-3">
            <h6 class="m-0 font-weight-bold text-primary"><a href="<?= base_url('AdditionalCosts') ?>"><i class="fas fa-arrow-left"></i> Kembali</a></h6>
        </div>
        <div class="card-body">
            <form action="" method="post" enctype="multipart/form-data">
                <input type="hidden" name="id" value="<?= $additionalcosts['id']; ?>" />
                <input type="hidden" name="category_id" value="<?= $additionalcosts['category_id']; ?>" />
                <input type="hidden" name="sub_category_id" value="<?= $additionalcosts['sub_category_id']; ?>" />
                <input type="hidden" name="investment_costs" value="<?= $additionalcosts['investment_costs']; ?>" />
                <input type="hidden" name="created_at" value="<?= $additionalcosts['created_at']; ?>" />
                <label for="costs_year">Biaya / Tahun</label>
                <input class="form-control" type="text" name="costs_year" placeholder="Biaya / Tahun" value="<?= $additionalcosts['costs_year'] ?>" />
                <?= form_error('costs_year', '<small class="text-danger pl-3">', '</small>'); ?>
                <!-- btn -->
                <input class="btn btn-success mt-4" type="submit" name="btn" value="Ubah" />
            </form>
        </div>
    </div>

</div>
<!-- /.container-fluid -->

</div>
<!-- End of Main Content -->
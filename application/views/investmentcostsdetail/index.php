<script>
    function deleteConfirm(url){
        $('#btn-delete').attr('href', url);
        $('#deleteModal').modal();
    }
</script>

<script>
    function editModalKerja(url){
        $('#btn-edit').attr('href', url);
        $('#editModal').modal();
    }
</script>

<!-- Begin Page Content -->
<div class="container-fluid">
  <!-- Page Heading -->
    <h1 class="h3 mb-4 text-gray-800"><?= $title; ?></h1>
    <!-- <a href="<?= base_url('InvestmentCostsDetail/excel'); ?>" class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm mb-2"><i class="fas fa-download fa-sm text-white-50"></i> Export Data</a> -->
    <div class="col-lg-7">
        <?= form_error('title', '<div class="alert alert-danger" role="alert">', '</div>'); ?>
        <?= $this->session->flashdata('message'); ?>
    </div>

  <div class="row">
    <!-- sub total -->
    <div class="col-xl-4 col-md-6 mb-4">
      <div class="card border-left-secondary shadow h-100 py-2">
        <div class="card-body">
          <div class="row no-gutters align-items-center">
            <div class="col mr-2">
              <div class="text-xs font-weight-bold text-secondary text-uppercase mb-1">Pengadaan Lahan</div>
              <div class="h5 mb-0 font-weight-bold text-gray-800"><?= "Rp." . number_format($stpengadaan_lahan); ?></div>
            </div>
            <div class="col-auto">
              <i class="fas fa-file-contract fa-2x text-gray-300"></i>
            </div>
          </div>
        </div>
      </div>
    </div>

    <!-- sub total -->
    <div class="col-xl-4 col-md-6 mb-4">
      <div class="card border-left-secondary shadow h-100 py-2">
        <div class="card-body">
          <div class="row no-gutters align-items-center">
            <div class="col mr-2">
              <div class="text-xs font-weight-bold text-secondary text-uppercase mb-1">Bangunan</div>
              <div class="h5 mb-0 font-weight-bold text-gray-800"><?= "Rp." . number_format($stbangunan); ?></div>
            </div>
            <div class="col-auto">
              <i class="fas fa-file-contract fa-2x text-gray-300"></i>
            </div>
          </div>
        </div>
      </div>
    </div>

    <!-- sub total -->
    <div class="col-xl-4 col-md-6 mb-4">
      <div class="card border-left-secondary shadow h-100 py-2">
        <div class="card-body">
          <div class="row no-gutters align-items-center">
            <div class="col mr-2">
              <div class="text-xs font-weight-bold text-secondary text-uppercase mb-1">Mesin & Peralatan</div>
              <div class="h5 mb-0 font-weight-bold text-gray-800"><?= "Rp." . number_format($stmesin); ?></div>
            </div>
            <div class="col-auto">
              <i class="fas fa-file-contract fa-2x text-gray-300"></i>
            </div>
          </div>
        </div>
      </div>
    </div>

    <!-- sub total -->
    <div class="col-xl-4 col-md-6 mb-4">
      <div class="card border-left-secondary shadow h-100 py-2">
        <div class="card-body">
          <div class="row no-gutters align-items-center">
            <div class="col mr-2">
              <div class="text-xs font-weight-bold text-secondary text-uppercase mb-1">Fasilitas</div>
              <div class="h5 mb-0 font-weight-bold text-gray-800"><?= "Rp." . number_format($stfasilitas); ?></div>
            </div>
            <div class="col-auto">
              <i class="fas fa-file-contract fa-2x text-gray-300"></i>
            </div>
          </div>
        </div>
      </div>
    </div>

    <!-- sub total -->
    <div class="col-xl-4 col-md-6 mb-4">
      <div class="card border-left-secondary shadow h-100 py-2">
        <div class="card-body">
          <div class="row no-gutters align-items-center">
            <div class="col mr-2">
              <div class="text-xs font-weight-bold text-secondary text-uppercase mb-1">Kendaraan</div>
              <div class="h5 mb-0 font-weight-bold text-gray-800"><?= "Rp." . number_format($stkendaraan); ?></div>
            </div>
            <div class="col-auto">
              <i class="fas fa-file-contract fa-2x text-gray-300"></i>
            </div>
          </div>
        </div>
      </div>
    </div>

    <!-- sub total -->
    <div class="col-xl-4 col-md-6 mb-4">
      <div class="card border-left-secondary shadow h-100 py-2">
        <div class="card-body">
          <div class="row no-gutters align-items-center">
            <div class="col mr-2">
              <div class="text-xs font-weight-bold text-secondary text-uppercase mb-1">Pra-Investasi</div>
              <div class="h5 mb-0 font-weight-bold text-gray-800"><?= "Rp." . number_format($stprainvest); ?></div>
            </div>
            <div class="col-auto">
              <i class="fas fa-file-contract fa-2x text-gray-300"></i>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

  <!-- Content Row -->
  <div class="row"> 
    <!-- total invest -->
    <div class="col-xl-3 col-md-6 mb-4">
      <div class="card border-left-primary shadow h-100 py-2">
        <div class="card-body">
          <div class="row no-gutters align-items-center">
            <div class="col mr-2">
              <div class="text-xs font-weight-bold text-primary text-uppercase mb-1">Investasi</div>
              <div class="h5 mb-0 font-weight-bold text-gray-800"><?= "Rp." . number_format($totalinvestasi); ?></div>
            </div>
            <div class="col-auto">
              <i class="fas fa-file-contract fa-2x text-gray-300"></i>
            </div>
          </div>
        </div>
      </div>
    </div>

    <!-- modal kerja -->
    <div class="col-xl-3 col-md-6 mb-4">
      <div class="card border-left-success shadow h-100 py-2">
        <div class="card-body">
          <div class="row no-gutters align-items-center">
            <div class="col mr-2">
              <div class="text-xs font-weight-bold text-success text-uppercase mb-1">Modal Kerja Awal</div>
              <div class="h5 mb-0 font-weight-bold text-gray-800"><?= "Rp." . number_format($modalkerja['subtotal']); ?></div>
            </div>
            <div class="col-auto">
              <i class="fas fa-file-contract fa-2x text-gray-300"></i>
            </div>
          </div>
        </div>
            <div class="col-lg-4 ml-2">
              <div class="small">
                <a class="badge badge-success" href="<?= site_url('InvestmentCostsDetail/editmodalkerja/'.$modalkerja['id']); ?>">Ubah</a>
              </div>
            </div>
      </div>
    </div>

    <div class="col-xl-3 col-md-6 mb-4">
      <div class="card border-left-info shadow h-100 py-2">
        <div class="card-body">
          <div class="row no-gutters align-items-center">
            <div class="col mr-2">
              <div class="text-xs font-weight-bold text-info text-uppercase mb-1">Kontingensi</div>
              <div class="h5 mb-0 font-weight-bold text-gray-800"><?= "Rp." . number_format($kotingensi) ?></div>
            </div>
            <div class="col-auto">
              <i class="fas fa-file-contract fa-2x text-gray-300"></i>
            </div>
          </div>
        </div>
      </div>
    </div>

    <!-- Pending Requests Card Example -->
    <div class="col-xl-3 col-md-6 mb-4">
      <div class="card border-left-warning shadow h-100 py-2">
        <div class="card-body">
          <div class="row no-gutters align-items-center">
            <div class="col mr-2">
              <div class="text-xs font-weight-bold text-warning text-uppercase mb-1">Total Biaya Investasi</div>
              <div class="h5 mb-0 font-weight-bold text-gray-800"><?= "Rp." . number_format($totalbaiayainvest); ?></div>
            </div>
            <div class="col-auto">
              <i class="fas fa-file-contract fa-2x text-gray-300"></i>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

    <div class="card col-lg-12 shadow mb-4">
        <div class="card-header py-3">
            <h6 class="m-0 font-weight-bold text-primary"><a href="" data-toggle="modal" data-target="#newInvestmenCostsDetailModal"><i class="fas fa-plus"></i> Tambah Biaya Investasi</a></h6>
        </div>
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table table-bordered" id="eksport" width="100%" cellspacing="0">
                        <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Kategori</th>
                                    <th>Sub Kategori</th>
                                    <th>Satuan</th>
                                    <th>Jumlah</th>
                                    <th>Harga Satuan</th>
                                    <th>Sub Total</th>
                                    <th>Aksi</th>
                                </tr>
                        </thead>
                    <tbody>
                        <?php $index = 1; ?>
                        <?php foreach($investmentcostsdetail as $icd) : ?>
                        <tr>
                            <td><?= $index; ?></td>
                            <td><?= $icd['title_c']; ?></td>
                            <td><?= $icd['title_sc']; ?></td>
                            <td><?= $icd['title_u']; ?></td>
                            <td><?= $icd['total']; ?></td>
                            <td><?= "Rp." . number_format($icd['price_per_unit']); ?></td>
                            <td><?= "Rp." . number_format($icd['subtotal']); ?></td>
                            <td>
                                <a class="badge badge-success" href="<?= site_url('InvestmentCostsDetail/edit/'.$icd['id']); ?>">Ubah</a>
                                <a class="badge badge-danger" href="#!" onclick="deleteConfirm('<?= site_url('InvestmentCostsDetail/delete/'.$icd['id']); ?>')">Hapus</a>
                            </td>
                        </tr>
                        <?php $index++; ?>
                        <?php endforeach; ?>
                    </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
<!-- /.container-fluid -->
</div>
<!-- Modal add new unit-->
<div class="modal fade" id="newInvestmenCostsDetailModal" tabindex="-1" role="dialog" aria-labelledby="newInvestmenCostsDetailModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="newInvestmenCostsDetailModalLabel">Tambah Biaya Investasi</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <!-- form -->
      <form action="<?= site_url('InvestmentCostsDetail/add'); ?>" method="post">
        <div class="modal-body">
          <div class="form-group row">
            <!-- input category -->
            <div class="col-lg-4">
              <label for="category_id">Kategori</label>
              <select name="category_id" id="category_id" class="form-control">
                  <option value="">Pilih Kategori</option>
                  <?php foreach ($category as $c) : ?>
                  <option value="<?= $c['id']; ?>"><?= $c['title']; ?></option>
                  <?php endforeach; ?>
              </select>
            </div>
            <!-- input sub category -->
            <div class="col-lg-4">
              <label for="sub_category_id">Sub Kategori</label>
              <select name="sub_category_id" id="sub_category_id" class="form-control">
                  <option value="">Pilih Sub Kategori</option>
              </select>
            </div>
            <!-- input unit -->
            <div class="col-lg-4">
              <label for="unit_id">Satuan</label>
              <select name="unit_id" id="unit_id" class="form-control">
                  <option value="1">-</option>
                  <?php foreach ($unit as $u) : ?>
                  <option value="<?= $u['id']; ?>"><?= $u['title']; ?></option>
                  <?php endforeach; ?>
              </select>
            </div>
          </div>
          <div class="form-group row">
            <!-- input total -->
            <div class="col-lg-6">
              <label for="total">Jumlah</label>
              <input type="text" class="form-control" id="total" name="total" placeholder="Jumlah" value="0">
            </div>
            <!-- input price per unit -->
            <div class="col-lg-6">
              <label for="price_per_unit">Harga Satuan</label>
              <input type="text" class="form-control" id="price_per_unit" name="price_per_unit" placeholder="Harga Satuan">
              <?= form_error('price_per_unit', '<small class="text-danger pl-3">', '</small>'); ?>
            </div>
          </div>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
            <button type="submit" class="btn btn-primary">Tambah</button>
        </div>
      </form>
        <script src="https://code.jquery.com/jquery-3.5.1.min.js" integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0=" crossorigin="anonymous"></script>
      	<script>
          // when page is ready
          $(document).ready(function(){
            // when user selected
            $("#category_id").change(function(){
              $("#sub_category_id").hide(); // hide combo box
              // ajax
              $.ajax({
                type: "POST", //post method
                url: "<?php echo base_url("InvestmentCostsDetail/listSubCategory"); ?>", // url controller
                data: {id_category : $("#category_id").val()}, // send data
                dataType: "json",
                beforeSend: function(e) {
                  if(e && e.overrideMimeType) {
                    e.overrideMimeType("application/json;charset=UTF-8");
                  }
                },
                success: function(response){ // process success
                  // set combo box
                  // show combo box
                  $("#sub_category_id").html(response.list_sub_category).show();
                },
                // handle error
                error: function (xhr, ajaxOptions, thrownError) {
                  alert(xhr.status + "\n" + xhr.responseText + "\n" + thrownError); // show alert
                }
              });
            });
          });
        </script>
    </div>
  </div>
</div>

<!-- modal delete -->
<div class="modal fade" id="deleteModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Apa Anda Yakin?</h5>
        <button class="close" type="button" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">×</span>
        </button>
      </div>
      <div class="modal-body">Data yang sudah dihapus tidak dapat dikembalikan!</div>
      <div class="modal-footer">
        <button class="btn btn-secondary" type="button" data-dismiss="modal">Batal</button>
        <a id="btn-delete" class="btn btn-danger" href="#">Hapus</a>
      </div>
    </div>
  </div>
</div>

<!-- modal edit -->
<div class="modal fade" id="editModalKerja" tabindex="-1" role="dialog" aria-labelledby="editModalKerjaLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="editModalKerjaLabel">Ubah Modal Kerja</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <!-- form -->
      <form action="<?= site_url('InvestmentCostsDetail/editmodalkerja'); ?>" method="post">
        <div class="modal-body"> 
            <!-- input total -->
            <label for="total">Modal Kerja</label>
            <input type="text" class="form-control" id="total" name="total" placeholder="Jumlah" value="0">
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
            <button type="submit" class="btn btn-primary">Ubah</button>
        </div>
      </form>
    </div>
  </div>
</div>
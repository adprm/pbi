<!-- Begin Page Content -->
<div class="container-fluid">

    <!-- Page Heading -->
    <h1 class="h3 mb-4 text-gray-800"><?= $title; ?></h1>

    <div class="card col-lg-12 shadow mb-4">
        <div class="card-header py-3">
            <h6 class="m-0 font-weight-bold text-primary"><a href="<?= base_url('InvestmentCostsDetail') ?>"><i class="fas fa-arrow-left"></i> Kembali</a></h6>
        </div>
        <div class="card-body">
            <form action="" method="post" enctype="multipart/form-data">
                <input type="hidden" name="id" value="<?= $investmentcostsdetail['id']; ?>" />
                    <div class="form-group row">
                        <!-- input category -->
                        <div class="col-lg-4">
                            <label for="category_id">Kategori</label>
                            <select name="category_id" id="category_id" class="form-control">
                                <option value="">Pilih Kategori</option>
                                <?php foreach ($category as $c) : ?>
                                <option value="<?= $c['id']; ?>"><?= $c['title']; ?></option>
                                <?php endforeach; ?>
                            </select>
                            <?= form_error('category_id', '<small class="text-danger pl-3">', '</small>'); ?>
                        </div>
                        <!-- input sub category -->
                        <div class="col-lg-4">
                            <label for="sub_category_id">Sub Kategori</label>
                            <select name="sub_category_id" id="sub_category_id" class="form-control">
                                <option value="">Pilih Sub Kategori</option>
                            </select>
                            <?= form_error('sub_category_id', '<small class="text-danger pl-3">', '</small>'); ?>
                        </div>
                        <!-- input unit -->
                        <div class="col-lg-4">
                            <label for="unit_id">Satuan</label>
                            <select name="unit_id" id="unit_id" class="form-control">
                                <option value="1">-</option>
                                <?php foreach ($unit as $u) : ?>
                                <option value="<?= $u['id']; ?>"><?= $u['title']; ?></option>
                                <?php endforeach; ?>
                            </select>
                        </div>
                    </div>
                    <div class="form-group row">
                        <!-- input total -->
                        <div class="col-lg-6">
                            <label for="total">Jumlah</label>
                            <input class="form-control" type="text" name="total" placeholder="Jumlah" value="<?= $investmentcostsdetail['total'] ?>" />
                        </div>
                        <!-- input price per unit -->
                        <div class="col-lg-6">
                            <label for="price_per_unit">Harga Per Unit</label>
                            <input class="form-control" type="text" name="price_per_unit" placeholder="Harga Per Unit" value="<?= $investmentcostsdetail['price_per_unit'] ?>" />
                            <?= form_error('price_per_unit', '<small class="text-danger pl-3">', '</small>'); ?>
                        </div>
                    </div>
                <!-- btn -->
                <input class="btn btn-success" type="submit" name="btn" value="Ubah" />
            </form>
            <script src="https://code.jquery.com/jquery-3.5.1.min.js" integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0=" crossorigin="anonymous"></script>
            <script>
                // when page is ready
                $(document).ready(function(){
                    // when user selected
                    $("#category_id").change(function(){
                    $("#sub_category_id").hide(); // hide combo box
                    // ajax
                    $.ajax({
                        type: "POST", //post method
                        url: "<?php echo base_url("InvestmentCostsDetail/listSubCategory"); ?>", // url controller
                        data: {id_category : $("#category_id").val()}, // send data
                        dataType: "json",
                        beforeSend: function(e) {
                        if(e && e.overrideMimeType) {
                            e.overrideMimeType("application/json;charset=UTF-8");
                        }
                        },
                        success: function(response){ // process success
                        // set combo box
                        // show combo box
                        $("#sub_category_id").html(response.list_sub_category).show();
                        },
                        // handle error
                        error: function (xhr, ajaxOptions, thrownError) {
                        alert(xhr.status + "\n" + xhr.responseText + "\n" + thrownError); // show alert
                        }
                    });
                    });
                });
            </script>
        </div>
    </div>

</div>
<!-- /.container-fluid -->

</div>
<!-- End of Main Content -->
<!-- Begin Page Content -->
<div class="container-fluid">

    <!-- Page Heading -->
    <h1 class="h3 mb-4 text-gray-800"><?= $title; ?></h1>

    <div class="card col-lg-6 shadow mb-4">
        <div class="card-header py-3">
            <h6 class="m-0 font-weight-bold text-primary"><a href="<?= base_url('ProfitAnalysis') ?>"><i class="fas fa-arrow-left"></i> Kembali</a></h6>
        </div>
        <div class="card-body">
            <form action="" method="post" enctype="multipart/form-data">
                <input type="hidden" name="id" value="<?= $configuration['id']; ?>" />
                <input type="hidden" name="interest_loan_costs" value="<?= $configuration['interest_loan_costs']; ?>" />
                <input type="hidden" name="percent_pajak" value="<?= $configuration['percent_pajak']; ?>" />
                <div class="form-group row">
                    <!-- acceptance_value -->
                    <div class="col-lg-4">
                        <label for="acceptance_value">Nilai Penerimaan</label>
                        <input class="form-control" type="text" name="acceptance_value" value="<?= $configuration['acceptance_value'] ?>" />
                    </div>
                </div>
                <!-- btn -->
                <input class="btn btn-success" type="submit" name="btn" value="Ubah" />
            </form>
        </div>
    </div>

</div>
<!-- /.container-fluid -->

</div>
<!-- End of Main Content -->
<script>
    function deleteConfirm(url){
        $('#btn-delete').attr('href', url);
        $('#deleteModal').modal();
    }

</script>

<!-- Begin Page Content -->
<div class="container-fluid">

    <!-- Page Heading -->
    <h1 class="h3 mb-4 text-gray-800"><?= $title; ?></h1>
    <!-- <a href="<?= base_url('ProductionCosts/excel'); ?>" class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm mb-2"><i class="fas fa-download fa-sm text-white-50"></i> Export Data</a> -->

    <div class="card col-lg-12 shadow mb-4">
        <div class="card-header py-3">
            <h6 class="m-0 font-weight-bold text-primary">Biaya Produksi</h6>
        </div>
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table table-bordered" id="eksport" width="100%" cellspacing="0">
                    <!-- <table class="table table-bordered" id="eksport" width="100%" cellspacing="0"> -->
                        <thead>
                                <tr>
                                    <th>Ketegori</th>
                                    <th>Sub Ketegori</th>
                                    <th>Tahun 1</th> 
                                    <th>Tahun 2</th> 
                                    <th>Tahun 3</th> 
                                    <th>Tahun 4</th> 
                                    <th>Tahun 5</th> 
                                    <th>Tahun 6</th> 
                                    <th>Tahun 7</th> 
                                    <th>Tahun 8</th> 
                                    <th>Tahun 9</th> 
                                    <th>Tahun 10</th> 
                                </tr>
                        </thead>
                    <tbody>
                        <tr>
                            <td>Biaya Tetap</td>
                            <td>TK. Tidak Langsung</td>
                            <td><?= 'Rp.' . number_format($tktidaklangsung); ?></td>
                            <td><?= 'Rp.' . number_format($tktidaklangsung); ?></td>
                            <td><?= 'Rp.' . number_format($tktidaklangsung); ?></td>
                            <td><?= 'Rp.' . number_format($tktidaklangsung); ?></td>
                            <td><?= 'Rp.' . number_format($tktidaklangsung); ?></td>
                            <td><?= 'Rp.' . number_format($tktidaklangsung); ?></td>
                            <td><?= 'Rp.' . number_format($tktidaklangsung); ?></td>
                            <td><?= 'Rp.' . number_format($tktidaklangsung); ?></td>
                            <td><?= 'Rp.' . number_format($tktidaklangsung); ?></td>
                            <td><?= 'Rp.' . number_format($tktidaklangsung); ?></td>
                        </tr>
                        <tr>
                            <td>Biaya Tetap</td>
                            <td>Pemeliharaan</td>
                            <td><?= 'Rp.' . number_format($pemeliharaan); ?></td>
                            <td><?= 'Rp.' . number_format($pemeliharaan); ?></td>
                            <td><?= 'Rp.' . number_format($pemeliharaan); ?></td>
                            <td><?= 'Rp.' . number_format($pemeliharaan); ?></td>
                            <td><?= 'Rp.' . number_format($pemeliharaan); ?></td>
                            <td><?= 'Rp.' . number_format($pemeliharaan); ?></td>
                            <td><?= 'Rp.' . number_format($pemeliharaan); ?></td>
                            <td><?= 'Rp.' . number_format($pemeliharaan); ?></td>
                            <td><?= 'Rp.' . number_format($pemeliharaan); ?></td>
                            <td><?= 'Rp.' . number_format($pemeliharaan); ?></td>
                        </tr>
                        <tr>
                            <td>Biaya Tetap</td>
                            <td>Asuransi</td>
                            <td><?= 'Rp.' . number_format($asuransi); ?></td>
                            <td><?= 'Rp.' . number_format($asuransi); ?></td>
                            <td><?= 'Rp.' . number_format($asuransi); ?></td>
                            <td><?= 'Rp.' . number_format($asuransi); ?></td>
                            <td><?= 'Rp.' . number_format($asuransi); ?></td>
                            <td><?= 'Rp.' . number_format($asuransi); ?></td>
                            <td><?= 'Rp.' . number_format($asuransi); ?></td>
                            <td><?= 'Rp.' . number_format($asuransi); ?></td>
                            <td><?= 'Rp.' . number_format($asuransi); ?></td>
                            <td><?= 'Rp.' . number_format($asuransi); ?></td>
                        </tr>
                        <tr>
                            <td>Biaya Tetap</td>
                            <td>PBB</td>
                            <td><?= 'Rp.' . number_format($pajakbumi); ?></td>
                            <td><?= 'Rp.' . number_format($pajakbumi); ?></td>
                            <td><?= 'Rp.' . number_format($pajakbumi); ?></td>
                            <td><?= 'Rp.' . number_format($pajakbumi); ?></td>
                            <td><?= 'Rp.' . number_format($pajakbumi); ?></td>
                            <td><?= 'Rp.' . number_format($pajakbumi); ?></td>
                            <td><?= 'Rp.' . number_format($pajakbumi); ?></td>
                            <td><?= 'Rp.' . number_format($pajakbumi); ?></td>
                            <td><?= 'Rp.' . number_format($pajakbumi); ?></td>
                            <td><?= 'Rp.' . number_format($pajakbumi); ?></td>
                        </tr>
                        <tr>
                            <td>Biaya Tetap</td>
                            <td>Pemasaran</td>
                            <td><?= 'Rp.' . number_format($pemasaran['costs_year']); ?></td>
                            <td><?= 'Rp.' . number_format($pemasaran['costs_year']); ?></td>
                            <td><?= 'Rp.' . number_format($pemasaran['costs_year']); ?></td>
                            <td><?= 'Rp.' . number_format($pemasaran['costs_year']); ?></td>
                            <td><?= 'Rp.' . number_format($pemasaran['costs_year']); ?></td>
                            <td><?= 'Rp.' . number_format($pemasaran['costs_year']); ?></td>
                            <td><?= 'Rp.' . number_format($pemasaran['costs_year']); ?></td>
                            <td><?= 'Rp.' . number_format($pemasaran['costs_year']); ?></td>
                            <td><?= 'Rp.' . number_format($pemasaran['costs_year']); ?></td>
                            <td><?= 'Rp.' . number_format($pemasaran['costs_year']); ?></td>
                        </tr>
                        <tr>
                            <td>Biaya Tetap</td>
                            <td>Administrasi</td>
                            <td><?= 'Rp.' . number_format($administrasi['costs_year']); ?></td>
                            <td><?= 'Rp.' . number_format($administrasi['costs_year']); ?></td>
                            <td><?= 'Rp.' . number_format($administrasi['costs_year']); ?></td>
                            <td><?= 'Rp.' . number_format($administrasi['costs_year']); ?></td>
                            <td><?= 'Rp.' . number_format($administrasi['costs_year']); ?></td>
                            <td><?= 'Rp.' . number_format($administrasi['costs_year']); ?></td>
                            <td><?= 'Rp.' . number_format($administrasi['costs_year']); ?></td>
                            <td><?= 'Rp.' . number_format($administrasi['costs_year']); ?></td>
                            <td><?= 'Rp.' . number_format($administrasi['costs_year']); ?></td>
                            <td><?= 'Rp.' . number_format($administrasi['costs_year']); ?></td>
                        </tr>
                        <tr>
                            <td><b>Sub Total Biaya Tetap</b></td>
                            <td>-</td>
                            <td><?= 'Rp.' . number_format($totalbiayatetap); ?></td>
                            <td><?= 'Rp.' . number_format($totalbiayatetap); ?></td>
                            <td><?= 'Rp.' . number_format($totalbiayatetap); ?></td>
                            <td><?= 'Rp.' . number_format($totalbiayatetap); ?></td>
                            <td><?= 'Rp.' . number_format($totalbiayatetap); ?></td>
                            <td><?= 'Rp.' . number_format($totalbiayatetap); ?></td>
                            <td><?= 'Rp.' . number_format($totalbiayatetap); ?></td>
                            <td><?= 'Rp.' . number_format($totalbiayatetap); ?></td>
                            <td><?= 'Rp.' . number_format($totalbiayatetap); ?></td>
                            <td><?= 'Rp.' . number_format($totalbiayatetap); ?></td>
                        </tr>
                        <tr>
                            <td>Biaya Tidak Tetap</td>
                            <td>B. Baku & Input</td>
                            <td><?= 'Rp.' . number_format($bakuinput1); ?></td>
                            <td><?= 'Rp.' . number_format($bakuinput2); ?></td>
                            <td><?= 'Rp.' . number_format($bakuinput); ?></td>
                            <td><?= 'Rp.' . number_format($bakuinput); ?></td>
                            <td><?= 'Rp.' . number_format($bakuinput); ?></td>
                            <td><?= 'Rp.' . number_format($bakuinput); ?></td>
                            <td><?= 'Rp.' . number_format($bakuinput); ?></td>
                            <td><?= 'Rp.' . number_format($bakuinput); ?></td>
                            <td><?= 'Rp.' . number_format($bakuinput); ?></td>
                            <td><?= 'Rp.' . number_format($bakuinput); ?></td>
                        </tr>
                        <tr>
                            <td>Biaya Tidak Tetap</td>
                            <td>TK. Langsung</td>
                            <td><?= 'Rp.' . number_format($tklangsung1); ?></td>
                            <td><?= 'Rp.' . number_format($tklangsung2); ?></td>
                            <td><?= 'Rp.' . number_format($tklangsung); ?></td>
                            <td><?= 'Rp.' . number_format($tklangsung); ?></td>
                            <td><?= 'Rp.' . number_format($tklangsung); ?></td>
                            <td><?= 'Rp.' . number_format($tklangsung); ?></td>
                            <td><?= 'Rp.' . number_format($tklangsung); ?></td>
                            <td><?= 'Rp.' . number_format($tklangsung); ?></td>
                            <td><?= 'Rp.' . number_format($tklangsung); ?></td>
                            <td><?= 'Rp.' . number_format($tklangsung); ?></td>
                        </tr>
                        <tr>
                            <td><b>Sub Total Biaya Tidak Tetap</b></td>
                            <td>-</td>
                            <td><?= 'Rp.' . number_format($totalbiayatidaktetap1); ?></td>
                            <td><?= 'Rp.' . number_format($totalbiayatidaktetap2); ?></td>
                            <td><?= 'Rp.' . number_format($totalbiayatidaktetap3); ?></td>
                            <td><?= 'Rp.' . number_format($totalbiayatidaktetap3); ?></td>
                            <td><?= 'Rp.' . number_format($totalbiayatidaktetap3); ?></td>
                            <td><?= 'Rp.' . number_format($totalbiayatidaktetap3); ?></td>
                            <td><?= 'Rp.' . number_format($totalbiayatidaktetap3); ?></td>
                            <td><?= 'Rp.' . number_format($totalbiayatidaktetap3); ?></td>
                            <td><?= 'Rp.' . number_format($totalbiayatidaktetap3); ?></td>
                            <td><?= 'Rp.' . number_format($totalbiayatidaktetap3); ?></td>
                        </tr>
                        <tr>
                            <td><b>Sub Total Biaya Produksi</b></td>
                            <td>-</td>
                            <td><?= 'Rp.' . number_format($totalBP1); ?></td>
                            <td><?= 'Rp.' . number_format($totalBP2); ?></td>
                            <td><?= 'Rp.' . number_format($totalBP3); ?></td>
                            <td><?= 'Rp.' . number_format($totalBP3); ?></td>
                            <td><?= 'Rp.' . number_format($totalBP3); ?></td>
                            <td><?= 'Rp.' . number_format($totalBP3); ?></td>
                            <td><?= 'Rp.' . number_format($totalBP3); ?></td>
                            <td><?= 'Rp.' . number_format($totalBP3); ?></td>
                            <td><?= 'Rp.' . number_format($totalBP3); ?></td>
                            <td><?= 'Rp.' . number_format($totalBP3); ?></td>
                        </tr>
                    </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
<!-- /.container-fluid -->
</div>
<!-- Modal add new profuctioncosts -->
<div class="modal fade" id="newProductionCostsModal" tabindex="-1" role="dialog" aria-labelledby="newProductionCostsModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="newProductionCostsModalLabel">Tambah Biaya Produksi</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <!-- form -->
      <form action="<?= site_url('ProductionCosts/add'); ?>" method="post">
        <div class="modal-body">
            <div class="form-group row">
                <div class="col-lg-6">
                  <label for="category_id">Kategori</label>
                  <select name="category_id" id="category_id" class="form-control">
                      <option value="">Pilih Kategori</option>
                      <?php foreach ($category as $c) : ?>
                          <option value="<?= $c['id']; ?>"><?= $c['title']; ?></option>
                      <?php endforeach; ?>
                  </select>
                </div>
                <div class="col-lg-6">
                  <label for="sub_category_id">Sub Kategori</label>
                    <select name="sub_category_id" id="sub_category_id" class="form-control">
                      <option value="">Pilih Sub Kategori</option>
                  </select>
                </div>
            </div>
            <div class="form-group row">
                <div class="col-lg-4">
                  <label for="year_1st">Tahun 1</label>
                  <input value="0" type="text" class="form-control" id="year_1st" name="year_1st" placeholder="Masukan Biaya">
                </div>
                <div class="col-lg-4">
                  <label for="year_2nd">Tahun 2</label>
                  <input value="0" type="text" class="form-control" id="year_2nd" name="year_2nd" placeholder="Masukan Biaya">
                </div>
                <div class="col-lg-4">
                  <label for="year_3rd">Tahun 3</label>
                  <input value="0" type="text" class="form-control" id="year_3rd" name="year_3rd" placeholder="Masukan Biaya">
                </div>
            </div>
            <div class="form-group row">
                <div class="col-lg-4">
                  <label for="year_4rd">Tahun 4</label>
                  <input value="0" type="text" class="form-control" id="year_4rd" name="year_4rd" placeholder="Masukan Biaya">
                </div>
                <div class="col-lg-4">
                  <label for="year_5rd">Tahun 5</label>
                  <input value="0" type="text" class="form-control" id="year_5rd" name="year_5rd" placeholder="Masukan Biaya">
                </div>
                <div class="col-lg-4">
                  <label for="year_6rd">Tahun 6</label>
                  <input value="0" type="text" class="form-control" id="year_6rd" name="year_6rd" placeholder="Masukan Biaya">
                </div>
            </div>
            <div class="form-group row">
                <div class="col-lg-4">
                  <label for="year_7rd">Tahun 7</label>
                  <input value="0" type="text" class="form-control" id="year_7rd" name="year_7rd" placeholder="Masukan Biaya">
                </div>
                <div class="col-lg-4">
                  <label for="year_8rd">Tahun 8</label>
                  <input value="0" type="text" class="form-control" id="year_8rd" name="year_8rd" placeholder="Masukan Biaya">
                </div>
                <div class="col-lg-4">
                  <label for="year_9rd">Tahun 9</label>
                  <input value="0" type="text" class="form-control" id="year_9rd" name="year_9rd" placeholder="Masukan Biaya">
                </div>
            </div>
            <div class="form-group row">
                <div class="col-lg-4">
                  <label for="year_10rd">Tahun 10</label>
                  <input value="0" type="text" class="form-control" id="year_10rd" name="year_10rd" placeholder="Masukan Biaya">
                </div>
            </div>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
            <button type="submit" class="btn btn-primary">Tambah</button>
        </div>
      </form>
      <script src="https://code.jquery.com/jquery-3.5.1.min.js" integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0=" crossorigin="anonymous"></script>
      	<script>
          // when page is ready
          $(document).ready(function(){
            // when user selected
            $("#category_id").change(function(){
              $("#sub_category_id").hide(); // hide combo box
              // ajax
              $.ajax({
                type: "POST", //post method
                url: "<?php echo base_url("InvestmentCostsDetail/listSubCategory"); ?>", // url controller
                data: {id_category : $("#category_id").val()}, // send data
                dataType: "json",
                beforeSend: function(e) {
                  if(e && e.overrideMimeType) {
                    e.overrideMimeType("application/json;charset=UTF-8");
                  }
                },
                success: function(response){ // process success
                  // set combo box
                  // show combo box
                  $("#sub_category_id").html(response.list_sub_category).show();
                },
                // handle error
                error: function (xhr, ajaxOptions, thrownError) {
                  alert(xhr.status + "\n" + xhr.responseText + "\n" + thrownError); // show alert
                }
              });
            });
          });
        </script>
    </div>
  </div>
</div>

<!-- modal delete -->
<div class="modal fade" id="deleteModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Apa Anda Yakin?</h5>
        <button class="close" type="button" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">×</span>
        </button>
      </div>
      <div class="modal-body">Data yang sudah dihapus tidak dapat dikembalikan!</div>
      <div class="modal-footer">
        <button class="btn btn-secondary" type="button" data-dismiss="modal">Batal</button>
        <a id="btn-delete" class="btn btn-danger" href="#">Hapus</a>
      </div>
    </div>
  </div>
</div>

<!-- <script src="https://code.jquery.com/jquery-3.5.1.min.js" integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0=" crossorigin="anonymous"> -->

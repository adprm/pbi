<script>
    function deleteConfirm(url){
        $('#btn-delete').attr('href', url);
        $('#deleteModal').modal();
    }
</script>

<!-- Begin Page Content -->
<div class="container-fluid">

    <!-- Page Heading -->
    <h1 class="h3 mb-4 text-gray-800"><?= $title; ?></h1>
    <!-- <a href="<?= base_url('MaterialCosts/excel'); ?>" class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm mb-2"><i class="fas fa-download fa-sm text-white-50"></i> Export Data</a> -->
    <div class="col-lg-7">
        <?= form_error('title', '<div class="alert alert-danger" role="alert">', '</div>'); ?>
        <?= $this->session->flashdata('message'); ?>
    </div>

    <div class="card col-lg-12 shadow mb-4">
        <div class="card-header py-3">
            <h6 class="m-0 font-weight-bold text-primary"><a href="" data-toggle="modal" data-target="#newMaterialCostsModal"><i class="fas fa-plus"></i> Tambah Biaya Bahan Baku & Utilitas</a></h6>
        </div>
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table table-bordered" id="eksport" width="100%" cellspacing="0">
                        <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Deskripsi</th>
                                    <th>Jumlah</th>
                                    <th>Harga Satuan</th>
                                    <th>Biaya / Tahun</th>
                                    <th>Aksi</th>
                                </tr>
                        </thead>
                        <tfoot>
                          <tr>
                            <th colspan="4">Total Biaya</th>
                            <th><?= "Rp." . number_format($stbahanbaku) ?></th>
                          </tr>
                        </tfoot>
                    <tbody>
                        <?php $index = 1; ?>
                        <?php foreach($materialcosts as $mc) : ?>
                        <tr>
                            <td><?= $index; ?></td>
                            <td><?= $mc['title']; ?></td>
                            <td><?= number_format($mc['total']); ?></td>
                            <td><?= "Rp." . number_format($mc['price_unit']); ?></td>
                            <td><?= "Rp." . number_format($mc['costs_year']); ?></td>
                            <td>
                                <a class="badge badge-success" href="<?= site_url('MaterialCosts/edit/'.$mc['id']); ?>">Ubah</a>
                                <a class="badge badge-danger" href="#!" onclick="deleteConfirm('<?= site_url('MaterialCosts/delete/'.$mc['id']); ?>')">Hapus</a>
                            </td>
                        </tr>
                        <?php $index++; ?>
                        <?php endforeach; ?>
                    </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
<!-- /.container-fluid -->
</div>
<!-- Modal add new materialcosts -->
<div class="modal fade" id="newMaterialCostsModal" tabindex="-1" role="dialog" aria-labelledby="newMaterialCostsModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="newMaterialCostsModalLabel">Tambah Biaya Baku & Utilitas</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <!-- form -->
      <form action="<?= site_url('MaterialCosts/add'); ?>" method="post">
        <div class="modal-body">
            <div class="form-group">
                <label for="title">Deskripsi</label>
                <input type="text" class="form-control" id="title" name="title" placeholder="Masukan Deskripsi">
                <?= form_error('title', '<small class="text-danger pl-3">', '</small>'); ?>
            </div>
            <div class="form-group">
                <label for="total">Jumlah</label>
                <input type="text" class="form-control" id="total" name="total" value="0" placeholder="Masukan Jumlah">
                <?= form_error('total', '<small class="text-danger pl-3">', '</small>'); ?>
            </div>
            <div class="form-group">
                <label for="price_unit">Harga Satuan</label>
                <input type="text" class="form-control" id="price_unit" name="price_unit" value="0" placeholder="Masukan Harga Satuan">
                <?= form_error('price_unit', '<small class="text-danger pl-3">', '</small>'); ?>
            </div>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
            <button type="submit" class="btn btn-primary">Tambah</button>
        </div>
      </form>
    </div>
  </div>
</div>

<!-- modal delete -->
<div class="modal fade" id="deleteModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Apa Anda Yakin?</h5>
        <button class="close" type="button" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">×</span>
        </button>
      </div>
      <div class="modal-body">Data yang sudah dihapus tidak dapat dikembalikan!</div>
      <div class="modal-footer">
        <button class="btn btn-secondary" type="button" data-dismiss="modal">Batal</button>
        <a id="btn-delete" class="btn btn-danger" href="#">Hapus</a>
      </div>
    </div>
  </div>
</div>
<script>
    function deleteConfirm(url){
        $('#btn-delete').attr('href', url);
        $('#deleteModal').modal();
    }
</script>

<!-- Begin Page Content -->
<div class="container-fluid">

    <!-- Page Heading -->
    <h1 class="h3 mb-4 text-gray-800"><?= $title; ?></h1>
    <div class="col-lg-7">
        <?= form_error('title', '<div class="alert alert-danger" role="alert">', '</div>'); ?>
        <?= $this->session->flashdata('message'); ?>
    </div>

    <div class="card col-lg-12 shadow mb-4">
        <div class="card-header py-3">
            <h6 class="m-0 font-weight-bold text-primary"><a href="" data-toggle="modal" data-target="#newCategoryModal"><i class="fas fa-plus"></i> Tambah Kategori</a></h6>
        </div>
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                        <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Tipe</th>
                                    <th>Kategori</th>
                                    <th>Aksi</th>
                                </tr>
                        </thead>
                    <tbody>
                        <?php $index = 1; ?>
                        <?php foreach($category as $c) : ?>
                        <tr>
                            <td><?= $index; ?></td>
                            <td>
                            <?php
                              if ($c['type'] === 'biaya_investasi') {
                                echo 'Biaya Investasi';
                              } else if ($c['type'] === 'biaya_lain') {
                                echo 'Biaya Lain-Lain';
                              } else if ($c['type'] === 'biaya_tenaga_kerja') {
                                echo 'Biaya Tenaga Kerja';
                              } else if ($c['type'] === 'biaya_produksi') {
                                echo 'Biaya Produksi';
                              } else if ($c['type'] === 'analisa_rugi_laba') {
                                echo 'Analisa Rugi Laba';
                              } else if ($c['type'] === 'arus_kas_pengeluaran_pemasukan') {
                                echo 'Arus Kas Pengeluaran Pemasukan';
                              }
                            ?>
                            </td>
                            <td><?= $c['title']; ?></td>
                            <td>
                                <a class="badge badge-success" href="<?= site_url('Category/edit/'.$c['id']); ?>">Ubah</a>
                                <!-- <a class="badge badge-danger" href="#!" onclick="deleteConfirm('<?= site_url('Category/delete/'.$c['id']); ?>')">Hapus</a> -->
                            </td>
                        </tr>
                        <?php $index++; ?>
                        <?php endforeach; ?>
                    </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
<!-- /.container-fluid -->
</div>
<!-- Modal add new category-->
<div class="modal fade" id="newCategoryModal" tabindex="-1" role="dialog" aria-labelledby="newCategoryModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="newCategoryModalLabel">Tambah Kategori</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <!-- form -->
      <form action="<?= site_url('Category/add'); ?>" method="post">
        <div class="modal-body">
            <div class="form-group">
                <label for="type">Tipe</label>
                <select name="type" id="type" class="form-control">
                    <option value="">Pilih Tipe</option>
                    <option value="biaya_investasi">Biaya Investasi</option>
                    <option value="biaya_Lain">Biaya Lain</option>
                    <option value="biaya_tenaga_kerja">Biaya Tenaga Kerja</option>
                    <option value="biaya_produksi">Biaya Produksi</option>
                    <option value="analisa_rugi_laba">Analisa Rugi Laba</option>
                    <option value="arus_kas_pengeluaran_pemasukan">Arus Kas Pengeluaran Pemasukan</option>
                    <?= form_error('type', '<small class="text-danger pl-3">', '</small>'); ?>
                </select>
            </div>
            <div class="form-group">
                <label for="title">Nama Kategori</label>
                <input type="text" class="form-control" id="title" name="title" placeholder="Kategori">
                <?= form_error('title', '<small class="text-danger pl-3">', '</small>'); ?>
            </div>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
            <button type="submit" class="btn btn-primary">Tambah</button>
        </div>
      </form>
    </div>
  </div>
</div>

<!-- modal delete -->
<div class="modal fade" id="deleteModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Apa Anda Yakin?</h5>
        <button class="close" type="button" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">×</span>
        </button>
      </div>
      <div class="modal-body">Data yang sudah dihapus tidak dapat dikembalikan!</div>
      <div class="modal-footer">
        <button class="btn btn-secondary" type="button" data-dismiss="modal">Batal</button>
        <a id="btn-delete" class="btn btn-danger" href="#">Hapus</a>
      </div>
    </div>
  </div>
</div>
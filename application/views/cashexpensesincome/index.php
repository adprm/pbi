<script>
    function deleteConfirm(url){
        $('#btn-delete').attr('href', url);
        $('#deleteModal').modal();
    }
</script>

<!-- Begin Page Content -->
<div class="container-fluid">

    <!-- Page Heading -->
    <h1 class="h3 mb-4 text-gray-800"><?= $title; ?></h1>
    <!--<a href="<?= base_url('ProductionCosts/excel'); ?>" class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm mb-2"><i class="fas fa-download fa-sm text-white-50"></i> Export Data</a>-->
    <div class="col-lg-7">
        <?= $this->session->flashdata('message'); ?>
    </div>

    <div class="card col-lg-12 shadow mb-4">
        <div class="card-header py-3">
            <h6 class="m-0 font-weight-bold text-primary"></i> Laporan Data Arus Kas</a></h6>
        </div>
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                        <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Ketegori</th>
                                    <th>Sub Kategori</th>
                                    <th>Tahun 0</th> 
                                    <th>Tahun 1</th> 
                                    <th>Tahun 2</th> 
                                    <th>Tahun 3</th> 
                                    <th>Tahun 4</th> 
                                    <th>Tahun 5</th> 
                                    <th>Tahun 6</th> 
                                    <th>Tahun 7</th> 
                                    <th>Tahun 8</th> 
                                    <th>Tahun 9</th> 
                                    <th>Tahun 10</th> 
                                </tr>
                        </thead>
                    <tbody>
                        <tr>
                            <td>1</td>
                            <td>Arus Kas Masuk</td>
                            <td>Laba Bersih</td>
                            <td>-</td>
                            <td><?= "Rp." . number_format($lababersih['lb1']) ?></td>
                            <td><?= "Rp." . number_format($lababersih['lb2']) ?></td>
                            <td><?= "Rp." . number_format($lababersih['lb3']) ?></td>
                            <td><?= "Rp." . number_format($lababersih['lb3']) ?></td>
                            <td><?= "Rp." . number_format($lababersih['lb3']) ?></td>
                            <td><?= "Rp." . number_format($lababersih['lb3']) ?></td>
                            <td><?= "Rp." . number_format($lababersih['lb3']) ?></td>
                            <td><?= "Rp." . number_format($lababersih['lb3']) ?></td>
                            <td><?= "Rp." . number_format($lababersih['lb3']) ?></td>
                            <td><?= "Rp." . number_format($lababersih['lb3']) ?></td>
                        </tr>
                        <tr>
                            <td>2</td>
                            <td>Arus Kas Masuk</td>
                            <td>Penyusutan</td>
                            <td>-</td>
                            <td><?= "Rp." . number_format($penyusutan) ?></td>
                            <td><?= "Rp." . number_format($penyusutan) ?></td>
                            <td><?= "Rp." . number_format($penyusutan) ?></td>
                            <td><?= "Rp." . number_format($penyusutan) ?></td>
                            <td><?= "Rp." . number_format($penyusutan) ?></td>
                            <td><?= "Rp." . number_format($penyusutan) ?></td>
                            <td><?= "Rp." . number_format($penyusutan) ?></td>
                            <td><?= "Rp." . number_format($penyusutan) ?></td>
                            <td><?= "Rp." . number_format($penyusutan) ?></td>
                            <td><?= "Rp." . number_format($penyusutan) ?></td>
                        </tr>
                        <tr>
                            <td>3</td>
                            <td>Arus Kas Masuk</td>
                            <td>Nilai Sisa</td>
                            <td>-</td>
                            <td>-</td>
                            <td>-</td>
                            <td>-</td>
                            <td>-</td>
                            <td><?= "Rp." . number_format($nilaisisa['residual_value']) ?></td>
                            <td>-</td>
                            <td>-</td>
                            <td>-</td>
                            <td>-</td>
                            <td><?= "Rp." . number_format($nilaisisa10) ?></td>
                        </tr>
                        <!-- <tr>
                            <td>4</td>
                            <td>Arus Kas Masuk</td>
                            <td>Md.Sendiri</td>
                            <td><?= "Rp." . number_format($mdsendiri['total']) ?></td>
                            <td>-</td>
                            <td>-</td>
                            <td>-</td>
                            <td>-</td>
                            <td>-</td>
                            <td>-</td>
                            <td>-</td>
                            <td>-</td>
                            <td>-</td>
                            <td>-</td>
                        </tr> -->
                        <?php $index = 4; ?>
                        <?php foreach($cashincome as $ci) : ?>
                        <tr>
                            <td><?= $index ?></td>
                            <td><?= $ci['title_c'] ?></td>
                            <td>
                                <?= $ci['title_sc'] ?>
                                <a class="badge badge-success" href="<?= site_url('CashExpensesIncome/edit/'.$ci['id']); ?>">Ubah</a>
                            </td>
                            <td><?= "Rp." . number_format($ci['zero_year']) ?></td>
                            <td><?= "Rp." . number_format($ci['1st_year']) ?></td>
                            <td><?= "Rp." . number_format($ci['2nd_year']) ?></td>
                            <td><?= "Rp." . number_format($ci['3rd_year']) ?></td>
                            <td><?= "Rp." . number_format($ci['4rd_year']) ?></td>
                            <td><?= "Rp." . number_format($ci['5rd_year']) ?></td>
                            <td><?= "Rp." . number_format($ci['6rd_year']) ?></td>
                            <td><?= "Rp." . number_format($ci['7rd_year']) ?></td>
                            <td><?= "Rp." . number_format($ci['8rd_year']) ?></td>
                            <td><?= "Rp." . number_format($ci['9rd_year']) ?></td>
                            <td><?= "Rp." . number_format($ci['10rd_year']) ?></td>
                        </tr>
                        <?php $index++ ?>
                        <?php endforeach; ?>
                        <tr>
                            <td>6</td>
                            <td colspan="2">Total Kas Masuk</td>
                            <td><?= "Rp." . number_format($tkm0) ?></td>
                            <td><?= "Rp." . number_format($tkm1) ?></td>
                            <td><?= "Rp." . number_format($tkm2) ?></td>
                            <td><?= "Rp." . number_format($tkm3) ?></td>
                            <td><?= "Rp." . number_format($tkm4) ?></td>
                            <td><?= "Rp." . number_format($tkm5) ?></td>
                            <td><?= "Rp." . number_format($tkm6) ?></td>
                            <td><?= "Rp." . number_format($tkm7) ?></td>
                            <td><?= "Rp." . number_format($tkm8) ?></td>
                            <td><?= "Rp." . number_format($tkm9) ?></td>
                            <td><?= "Rp." . number_format($tkm10) ?></td>
                        </tr>
                        <tr>
                            <td>7</td>
                            <td>Arus Kas Keluar</td>
                            <td>Investasi</td>
                            <td><?= "Rp." . number_format($tkm0) ?></td>
                            <td>-</td>
                            <td>-</td>
                            <td>-</td>
                            <td>-</td>
                            <td>-</td>
                            <td>-</td>
                            <td>-</td>
                            <td>-</td>
                            <td>-</td>
                            <td>-</td>
                        </tr>
                        <tr>
                            <td>8</td>
                            <td>Arus Kas Keluar</td>
                            <td>By. Produksi</td>
                            <td>-</td>
                            <td><?= "Rp." . number_format($kas['totalBP1']) ?></td>
                            <td><?= "Rp." . number_format($kas['totalBP2']) ?></td>
                            <td><?= "Rp." . number_format($kas['totalBP3']) ?></td>
                            <td><?= "Rp." . number_format($kas['totalBP3']) ?></td>
                            <td><?= "Rp." . number_format($kas['totalBP3']) ?></td>
                            <td><?= "Rp." . number_format($kas['totalBP3']) ?></td>
                            <td><?= "Rp." . number_format($kas['totalBP3']) ?></td>
                            <td><?= "Rp." . number_format($kas['totalBP3']) ?></td>
                            <td><?= "Rp." . number_format($kas['totalBP3']) ?></td>
                            <td><?= "Rp." . number_format($kas['totalBP3']) ?></td>
                        </tr>
                        <tr>
                            <td>8</td>
                            <td>Arus Kas Keluar</td>
                            <td>K. Pinjaman</td>
                            <td>-</td>
                            <td><?= "Rp." . number_format($kp1['total_instalment']) ?></td>
                            <td><?= "Rp." . number_format($kp2['total_instalment']) ?></td>
                            <td><?= "Rp." . number_format($kp3['total_instalment']) ?></td>
                            <td><?= "Rp." . number_format($kp4['total_instalment']) ?></td>
                            <td><?= "Rp." . number_format($kp5['total_instalment']) ?></td>
                            <td><?= "Rp." . number_format($kp6['total_instalment']) ?></td>
                            <td><?= "Rp." . number_format($kp7['total_instalment']) ?></td>
                            <td><?= "Rp." . number_format($kp8['total_instalment']) ?></td>
                            <td><?= "Rp." . number_format($kp9['total_instalment']) ?></td>
                            <td><?= "Rp." . number_format($kp10['total_instalment']) ?></td>
                        </tr>
                        <tr>
                            <td>9</td>
                            <td colspan="2">Total Kas Keluar</td>
                            <td><?= "Rp." . number_format($tkm0) ?></td>
                            <td><?= "Rp." . number_format($tkk1) ?></td>
                            <td><?= "Rp." . number_format($tkk2) ?></td>
                            <td><?= "Rp." . number_format($tkk3) ?></td>
                            <td><?= "Rp." . number_format($tkk4) ?></td>
                            <td><?= "Rp." . number_format($tkk5) ?></td>
                            <td><?= "Rp." . number_format($tkk6) ?></td>
                            <td><?= "Rp." . number_format($tkk7) ?></td>
                            <td><?= "Rp." . number_format($tkk8) ?></td>
                            <td><?= "Rp." . number_format($tkk9) ?></td>
                            <td><?= "Rp." . number_format($tkk10) ?></td>
                        </tr>
                        <tr>
                            <td>10</td>
                            <td colspan="2">Saldo Kas</td>
                            <td>-</td>
                            <td><?= "Rp." . number_format($sk1) ?></td>
                            <td><?= "Rp." . number_format($sk2) ?></td>
                            <td><?= "Rp." . number_format($sk3) ?></td>
                            <td><?= "Rp." . number_format($sk4) ?></td>
                            <td><?= "Rp." . number_format($sk5) ?></td>
                            <td><?= "Rp." . number_format($sk6) ?></td>
                            <td><?= "Rp." . number_format($sk7) ?></td>
                            <td><?= "Rp." . number_format($sk8) ?></td>
                            <td><?= "Rp." . number_format($sk9) ?></td>
                            <td><?= "Rp." . number_format($sk10) ?></td>
                        </tr>
                        <tr>
                            <td>11</td>
                            <td colspan="2">Saldo Kas Komulatif</td>
                            <td>-</td>
                            <td>-</td>
                            <td><?= "Rp." . number_format($skk2) ?></td>
                            <td><?= "Rp." . number_format($skk3) ?></td>
                            <td><?= "Rp." . number_format($skk4) ?></td>
                            <td><?= "Rp." . number_format($skk5) ?></td>
                            <td><?= "Rp." . number_format($skk6) ?></td>
                            <td><?= "Rp." . number_format($skk7) ?></td>
                            <td><?= "Rp." . number_format($skk8) ?></td>
                            <td><?= "Rp." . number_format($skk9) ?></td>
                            <td><?= "Rp." . number_format($skk10) ?></td>
                        </tr>
                    </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
<!-- /.container-fluid -->
</div>
<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class SubCategory_Model extends CI_Model {
    // method getAll
    public function getAll()
    {
        $query = "SELECT `sc`.*, `c`.`title` AS `titlec`
                FROM `sub_category` AS `sc` JOIN `category` AS `c`
                ON `sc`.`category_id` = `c`.`id`
                ORDER BY `sc`.`id` DESC";
        return $this->db->query($query)->result_array();
    }

    // get sub category by category id
    public function ViewByCategory($id_category)
    {
        $this->db->where('category_id', $id_category);
        $result = $this->db->get('sub_category')->result_array();
        return $result;
    }

    // method getById
    public function getById($id)
    {
        return $this->db->get_where('sub_category', ['id' => $id])->row_array();
    }

    // method save
    public function save()
    {
        date_default_timezone_set("Asia/Jakarta");
        $post = $this->input->post();
        $this->category_id = $post['category_id'];
        $this->title = $post['title'];
        $this->created_by = $post['created_by'] = 'admin';
        $this->created_at = date("y-m-d h:i:sa");
        $this->updated_at = date("y-m-d h:i:sa");

        return $this->db->insert('sub_category', $this);
    }

    // method update
    public function update()
    {
        date_default_timezone_set("Asia/Jakarta");
        $post = $this->input->post();
        $this->id = $post['id'];
        $this->category_id = $post['category_id'];
        $this->title = $post['title'];
        $this->created_by = $post['created_by'];
        $this->created_at = $post['created_at'];
        $this->updated_at = date("y-m-d h:i:sa");

        return $this->db->update('sub_category', $this, ['id' => $post['id']]);
    }

    // method delete
    public function delete($id)
    {
        return $this->db->delete('sub_category', ['id' => $id]);
    }
}
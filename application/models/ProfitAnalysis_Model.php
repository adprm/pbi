<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class ProfitAnalysis_Model extends CI_Model {
    // method get
    public function getAll()
    {
        $query = "SELECT `pa`.*, `c`.`title` AS `title_c`, `sc`.`title` AS `title_sc`
                FROM `profit_analysis` AS `pa` 
                JOIN `category` AS `c` ON `pa`.`category_id` = `c`.`id`
                JOIN `sub_category` AS `sc` ON `pa`.`sub_category_id` = `sc`.`id`
                ORDER BY `pa`.`id` DESC";

        return $this->db->query($query)->result_array();
    }

    // method getById
    public function getById($id)
    {
        return $this->db->get_where('profit_analysis', ['id' => $id])->row_array();
    }

    // meethod save
    public function save()
    {
        date_default_timezone_set("Asia/Jakarta");
        $post = $this->input->post();
        $data = [
            'category_id' => $post['category_id'],
            'sub_category_id' => $post['sub_category_id'],
            '1st_year' => $post['1st_year'],
            '2nd_year' => $post['2nd_year'],
            '3rd_year' => $post['3rd_year'],
            '4rd_year' => $post['4rd_year'],
            'created_at' => date("y-m-d h:i:sa"),
            'updated_at' => date("y-m-d h:i:sa"),
            '5rd_year' => $post['5rd_year'],
            '6rd_year' => $post['6rd_year'],
            '7rd_year' => $post['7rd_year'],
            '8rd_year' => $post['8rd_year'],
            '9rd_year' => $post['9rd_year'],
            '10rd_year' => $post['10rd_year'],

        ];

        return $this->db->insert('profit_analysis', $data);
    }

    // method update
    public function update($data, $id)
    {
        return $this->db->update('profit_analysis', $data, ['id' => $id]);
    }

    // method delete
    public function delete($id)
    {
        return $this->db->delete('profit_analysis', ['id' => $id]);
    }

    // get data num rows
    public function TotalAnalysisCosts() {
        return $this->db->get('profit_analysis')->num_rows();
    }
}
<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class ReductionCosts_Model extends CI_Model {
    
    // method getAll
    public function getAll()
    {
        $query = "SELECT `rc`.*, `c`.`title` AS `title_c`
                FROM `reduction_costs` AS `rc` 
                JOIN `category` AS `c` ON `rc`.`category_id` = `c`.`id`
                ORDER BY `rc`.`id` DESC";

        return $this->db->query($query)->result_array();
    }

    // method getById
    public function getById($id)
    {
        return $this->db->get_where('reduction_costs', ['id' => $id])->row_array();
    }

    // method save
    public function save($data)
    {
        return $this->db->insert('reduction_costs', $data);
    }
    
    // method update
    public function update($data, $id)
    {
        return $this->db->update('reduction_costs', $data, ['id' => $id]);
    }

    // method delete
    public function delete($id)
    {
        return $this->db->delete('reduction_costs', ['id' => $id]);
    }

    // get data num rows
    public function TotalReductionCosts() {
        return $this->db->get('reduction_costs')->num_rows();
    }

}
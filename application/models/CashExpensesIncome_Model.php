<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class CashExpensesIncome_Model extends CI_Model {
    // get all
    public function getAll()
    {
        $query = "SELECT `ce`.*, `c`.`title` AS `title_c`, `sc`.`title` AS `title_sc`
                FROM `cash_expenses_income` AS `ce` 
                JOIN `category` AS `c` ON `ce`.`category_id` = `c`.`id`
                JOIN `sub_category` AS `sc` ON `ce`.`sub_category_id` = `sc`.`id`
                ORDER BY `ce`.`id` DESC";

        return $this->db->query($query)->result_array();
    }

    // get by id
    public function getById($id)
    {
        return $this->db->get_where('cash_expenses_income', ['id' => $id])->row_array();
    }

    // update
    public function update($data, $id)
    {
        return $this->db->update('cash_expenses_income', $data, ['id' => $id]);
    }
}